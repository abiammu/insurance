import { Vehicle } from './Vehicle';

export class Insurance {
    vehicle: Vehicle[];
    insurance_for: String;
    about_insurance_plan: String;
    insurance_category: {
        category: String;
        insurance_provider_name: String;
    }
    insurance_plan_year: String;
    sum_insured: String;
    yearly_premium: String;




}
