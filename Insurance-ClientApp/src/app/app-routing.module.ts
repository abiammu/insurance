import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginComponent } from './user-auth/login/login.component';
import { RegisterComponent } from './user-auth/register/register.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './authguard/auth.guard';
import { AddPlansComponent } from './insurance-provider/add-plans/add-plans.component';
import { AddCategoryComponent } from './insurance-provider/add-category/add-category.component';
import { ShowPlansComponent } from './insurance-holder/show-plans/show-plans.component';
import { BuyPlansComponent } from './insurance-holder/buy-plans/buy-plans.component';
import { MyPoliciesComponent } from './insurance-holder/my-policies/my-policies.component';
import { GetVehicleToClaimComponent } from './insurance-holder/get-vehicle-to-claim/get-vehicle-to-claim.component';
import { GetClaimRequestComponent } from './inspector/get-claim-request/get-claim-request.component';
import { InspectorDashboardComponent } from './inspector/inspector-dashboard/inspector-dashboard.component';
import { ProviderDashboardComponent } from './insurance-provider/provider-dashboard/provider-dashboard.component';
import { HolderDashboardComponent } from './insurance-holder/holder-dashboard/holder-dashboard.component';
import { SampleComponent } from './sample/sample.component';
import { ViewPlansComponent } from './insurance-holder/view-plans/view-plans.component';
import { ViewPlanComponent } from './insurance-provider/view-plan/view-plan.component';
import { PublicViewPlansComponent } from './home/public-view-plans/public-view-plans.component';
import { ClaimStatusComponent } from './insurance-holder/claim-status/claim-status.component';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import{PublicShowPlansComponent} from './home/public-show-plans/public-show-plans.component';
import{GetApprovedClaimsComponent}from'./inspector/get-approved-claims/get-approved-claims.component';
import{GetRejectedClaimsComponent}from'./inspector/get-rejected-claims/get-rejected-claims.component';
import { ProviderClaimStatusComponent } from './insurance-provider/provider-claim-status/provider-claim-status.component';
import { ShowPlanComponent } from './insurance-provider/show-plan/show-plan.component';
import { BlockchainDashboardComponent } from './blockchain-dashboard/blockchain-dashboard.component';
import { BlockDetailsComponent } from './blockchain-dashboard/block-details/block-details.component';
import { BlockCountComponent } from './blockchain-dashboard/block-count/block-count.component';
import { BlockDataComponent } from './blockchain-dashboard/block-data/block-data.component';
import { PublicPreLoginComponent } from './home/public-pre-login/public-pre-login.component';
import { BlockClaimComponent } from './blockchain-dashboard/block-claim/block-claim.component';
import { ProviderBlockclaimComponent } from './blockchain-dashboard/provider-blockclaim/provider-blockclaim.component';
import { InspectorClaimstatusComponent } from './blockchain-dashboard/inspector-claimstatus/inspector-claimstatus.component';
const routes: Routes = [
  { path: 'public-view-plans', component: PublicViewPlansComponent },
  { path: '', component: PublicShowPlansComponent },
  // { path: 'claims', component: GetClaimsComponent },
  { path: 'userhome', component: ShowPlansComponent ,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_holder'
  }},
  {path:'show-plan',component:ShowPlanComponent,canActivate: [AuthGuard], data:{
    expectedRole:'insurance_provider'
  }},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent},
  /* Insurance Provider */
  { path: 'view-plan', component: ViewPlanComponent,canActivate: [AuthGuard],data:{
    expectedRole:'insurance_provider'
  } },
  { path: 'provider-dashboard', component: ProviderDashboardComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_provider'
  }},
  { path: 'add-plans', component: AddPlansComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_provider'
  } },
  { path: 'add-category', component: AddCategoryComponent,canActivate: [AuthGuard],data:{
    expectedRole:'insurance_provider'
  } },
  {path:'provider-claim-status',component:ProviderClaimStatusComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_provider'
  }},
  /* Insurance Holder */
  { path: 'holder-dashboard', component: HolderDashboardComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_holder'
  }},
  { path: 'buy-plans', component: BuyPlansComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_holder'
  }},
  { path: 'my-policies', component: MyPoliciesComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_holder'
  }},
  { path: 'get-vehicle-to-claim', component: GetVehicleToClaimComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_holder'
  }},
  { path: 'view-plans', component: ViewPlansComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_holder'
  }},
  { path: 'claim-insurance', component: GetVehicleToClaimComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_holder'
  }},
  { path: 'claim-status', component: ClaimStatusComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'insurance_holder'
  }},
  /* Inspector */
  { path: 'inspector-dashboard', component: InspectorDashboardComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'inspector'
  }},
  { path: 'claim-request', component: GetClaimRequestComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'inspector'
  }},
  { path: 'approved-claims', component: GetApprovedClaimsComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'inspector'
  }},
  { path: 'rejected-claims', component: GetRejectedClaimsComponent,canActivate: [AuthGuard] ,data:{
    expectedRole:'inspector'
  }},
  {path:'blockchain-dashboard',component:BlockchainDashboardComponent},
  {path:'block-details',component:BlockDetailsComponent},
  {path:'block-count',component:BlockCountComponent},
  {path:'block-data',component:BlockDataComponent},
  {path:'public-pre-login',component:PublicPreLoginComponent},
  {path:'block-claim',component:BlockClaimComponent},
  {path:'provider-blockclaim',component:ProviderBlockclaimComponent},
  {path:'inspector-claimstatus',component:InspectorClaimstatusComponent},
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
