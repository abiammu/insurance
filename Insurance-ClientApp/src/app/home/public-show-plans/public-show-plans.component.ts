import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-public-show-plans',
  templateUrl: './public-show-plans.component.html',
  styleUrls: ['./public-show-plans.component.css']
})
export class PublicShowPlansComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  viewinsurance(insurance_for) {
    localStorage.setItem('insurance_for', insurance_for);
    this.router.navigate(['/public-view-plans']);
  }
}
