import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicPreLoginComponent } from './public-pre-login.component';

describe('PublicPreLoginComponent', () => {
  let component: PublicPreLoginComponent;
  let fixture: ComponentFixture<PublicPreLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicPreLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicPreLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
