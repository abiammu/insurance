import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-public-pre-login',
  templateUrl: './public-pre-login.component.html',
  styleUrls: ['./public-pre-login.component.scss']
})
export class PublicPreLoginComponent implements OnInit {
  getCookieValue = {};

  /* Role Based Menu Items */
  policyHolder: boolean = false;
  insuranceProvider: boolean = false;
  inspector: boolean = false;
  loginRegister: boolean = false;
  logout: boolean = false;

  constructor(
      private router: Router
  ) {}

  ngOnInit() {
    this.getCookieValue = this.getlocalStorage('userData');

    this.initFunction();
  }
back(){
  this.router.navigate(['/'])
}
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }

  initFunction() {
    // handling menu items
    if (this.getCookieValue != null) {
      console.log('User has logged in');
      if (this.getCookieValue['role'] == 'insurance_holder') {
        this.policyHolder = true;
      } else if (this.getCookieValue['role'] == 'insurance_provider') {
        this.insuranceProvider = true;
      } else if (this.getCookieValue['role'] == 'inspector') {
        this.inspector = true;
      }
    } else {
      console.log('User is not logged in');
    }

    // handling login logout & register

    if (this.getCookieValue == null) {
      this.loginRegister = true;
      console.log('LoginReguster');
    } else if (this.getCookieValue !== null) {
      this.logout = true;
      console.log('Logout');
    }
  }

  logoutFunction() {
    localStorage.removeItem('userData');
    this.router.navigateByUrl('/login');
  }
}
