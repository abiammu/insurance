import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    private isLoggedIn: boolean;
    constructor(private router: Router, private _auth: AuthService) { }
    ngOnInit() {
        this.isLoggedIn;
    }
    records() {
        this.router.navigate(['./update']);

    }
    insurance() {
        this.router.navigate(['./add-category']);
    }
    logout() {
        this._auth.removeToken();
        this.isLoggedIn = false;
        this.router.navigateByUrl('');
    }
    users() {
        this.router.navigate(['./list-users']);
    }
    plans() {
        this.router.navigate(['./add-plans']);
    }
    schemes() {
        this.router.navigate(['./add-schemes']);
    }
}



