import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { Insurance } from 'src/app/Insurance';
import { Router } from '@angular/router';
@Component({
    selector: 'app-public-view-plans',
    templateUrl: './public-view-plans.component.html',
    styleUrls: ['./public-view-plans.component.css']
})
export class PublicViewPlansComponent implements OnInit {
    public insurances: Insurance[];
    getUserData = {};
    showDataFlag: boolean = false;

    constructor(private _auth: AuthService, private router: Router) { }

    ngOnInit() {
        this.viewinsurance();
        /* getting user data from cookie */
        //this.getUserData = this.getlocalStorage('userData').role;
        //console.log('getUserData', this.getUserData);
    }

    //getlocalStorage(cookieName) {
       // return JSON.parse(localStorage.getItem(cookieName));
    //}

    buy(plansId: any) {
        localStorage.setItem('planId', plansId);
        swal({

            text: "Please Login To Buy Insurance",
            icon: "warning",
        });
    }


    viewinsurance() {
        var getInsuranceFor = localStorage.getItem('insurance_for')
        this._auth.viewinsurance(getInsuranceFor).subscribe((data: Insurance[]) => {
            console.log('plan data:', data);
            if(data['payload'].length > 0){
                this.showDataFlag = true;
            }
            this.insurances = data;
            console.log(this.insurances);
        });
    }

}
