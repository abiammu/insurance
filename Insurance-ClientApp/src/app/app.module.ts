import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import { NgxLoadingModule } from 'ngx-loading';

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule
} from '@coreui/angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap';
//import { PopupModule } from 'ng2-opd-popup';
import { TooltipModule } from 'ngx-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './user-auth/register/register.component';
import { LoginComponent } from './user-auth/login/login.component';
import { AuthService } from './auth.service';
import { AuthGuard } from './authguard/auth.guard';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { AddPlansComponent } from './insurance-provider/add-plans/add-plans.component';
import { AddCategoryComponent } from './insurance-provider/add-category/add-category.component';
import { ShowPlansComponent } from './insurance-holder/show-plans/show-plans.component';
import { PublicViewPlansComponent } from './home/public-view-plans/public-view-plans.component';
import { ViewPlansComponent } from './insurance-holder/view-plans/view-plans.component';
import { ViewPlanComponent } from './insurance-provider/view-plan/view-plan.component';
import { BuyPlansComponent } from './insurance-holder/buy-plans/buy-plans.component';
import { MyPoliciesComponent } from './insurance-holder/my-policies/my-policies.component';
import { GetVehicleToClaimComponent } from './insurance-holder/get-vehicle-to-claim/get-vehicle-to-claim.component';
import { ClaimingReasonComponent } from './insurance-holder/claiming-reason/claiming-reason.component';
import { GetClaimRequestComponent } from './inspector/get-claim-request/get-claim-request.component';
import { HolderDashboardComponent } from './insurance-holder/holder-dashboard/holder-dashboard.component';
import { ProviderDashboardComponent } from './insurance-provider/provider-dashboard/provider-dashboard.component';
import { InspectorDashboardComponent } from './inspector/inspector-dashboard/inspector-dashboard.component';
import { SampleComponent } from './sample/sample.component';
import { PreLoginComponent } from './navigation/pre-login/pre-login.component';
import { PolicyHolderNavComponent } from './insurance-holder/policy-holder-nav/policy-holder-nav.component';
import { InsuranceProviderNavComponent } from './insurance-provider/insurance-provider-nav/insurance-provider-nav.component';
import { InspectorNavComponent } from './inspector/inspector-nav/inspector-nav.component';
import { ClaimStatusComponent } from './insurance-holder/claim-status/claim-status.component';
import{PublicShowPlansComponent} from './home/public-show-plans/public-show-plans.component';
import{GetApprovedClaimsComponent}from './inspector/get-approved-claims/get-approved-claims.component';
import{GetRejectedClaimsComponent}from './inspector/get-rejected-claims/get-rejected-claims.component';
import { ProviderClaimStatusComponent } from './insurance-provider/provider-claim-status/provider-claim-status.component';
import { ShowPlanComponent } from './insurance-provider/show-plan/show-plan.component';
import { BlockchainDashboardComponent } from './blockchain-dashboard/blockchain-dashboard.component';
import { BlockDetailsComponent } from './blockchain-dashboard/block-details/block-details.component';
import { BlockHashComponent } from './blockchain-dashboard/block-hash/block-hash.component';
import { BlockCountComponent } from './blockchain-dashboard/block-count/block-count.component';
import { BlockDataComponent } from './blockchain-dashboard/block-data/block-data.component';
import { PublicPreLoginComponent } from './home/public-pre-login/public-pre-login.component';
import { BlockClaimComponent } from './blockchain-dashboard/block-claim/block-claim.component';
import { ProviderBlockclaimComponent } from './blockchain-dashboard/provider-blockclaim/provider-blockclaim.component';
import { InspectorClaimstatusComponent } from './blockchain-dashboard/inspector-claimstatus/inspector-claimstatus.component';
@NgModule({
  declarations: [
    // GetClaimsComponent,
    ViewPlanComponent,
    GetRejectedClaimsComponent,
    GetApprovedClaimsComponent,
    PublicViewPlansComponent,
    AppComponent,
    PublicShowPlansComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    AddPlansComponent,
    AddCategoryComponent,
    ShowPlansComponent,
    ViewPlansComponent,
    BuyPlansComponent,
    MyPoliciesComponent,
    GetVehicleToClaimComponent,
    GetClaimRequestComponent,
    ClaimingReasonComponent,
    HolderDashboardComponent,
    ProviderDashboardComponent,
    InspectorDashboardComponent,
    SampleComponent,
    PreLoginComponent,
    PolicyHolderNavComponent,
    InsuranceProviderNavComponent,
    InspectorNavComponent,
    ClaimStatusComponent,
    ProviderClaimStatusComponent,
    ShowPlanComponent,
    BlockchainDashboardComponent,
    BlockDetailsComponent,
    BlockHashComponent,
    BlockCountComponent,
    BlockDataComponent,
    PublicPreLoginComponent,
    BlockClaimComponent,
    ProviderBlockclaimComponent,
    InspectorClaimstatusComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    NgxPasswordToggleModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    
    AppFooterModule,
    AppHeaderModule,
    NgxLoadingModule,
    AppSidebarModule,
    NgbModule.forRoot(),
    TooltipModule.forRoot(),
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),

    //PopupModule.forRoot()
  ],
  providers: [AuthService, AuthGuard, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule {}
