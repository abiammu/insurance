import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  category: any;

  //private _registerUrl = "indiuminsurance.ml/api/insurance-portal/register/user";
  //private _loginUrl = "indiuminsurance.ml/api/insurance-portal/login/user";
  private _vehicleUrl =
    'indiuminsurance.ml/api/insurance-portal/add-vehicle';
  private _userUrl = 'indiuminsurance.ml/api/insurance-portal/allUsers';
  private _getpoliciesUrl =
    'indiuminsurance.ml/api/insurance-portal/user-getInsured-data';
  private _getvehicleUrl =
    'indiuminsurance.ml/api/insurance-portal/user-claim-insurance-vehicle-data';
  private _insurerUrl =
    'indiuminsurance.ml/api/insurance-portal/register-insurer';
  private _insurercategoryUrl =
    'indiuminsurance.ml/api/insurance-portal/insurer-addCategory';
  private _planurl =
    'indiuminsurance.ml/api/insurance-portal/insurer-addPlans';
  private _buyplansUrl =
    'indiuminsurance.ml/api/insurance-portal/user-insure';
  private _viewplansUrl = 'indiuminsurance.ml/api/insurance-portal/getplans';

  private _viewinsurerplansUrl = 'indiuminsurance.ml/api/insurance-portal/insurer-getplans';
  private _claimUrl =
    'indiuminsurance.ml/api/insurance-portal/user-claim-insurance-vehicle-data';
  
  public flagDummy: Boolean = false;

  constructor(private http: HttpClient, private _router: Router) {}

  //addUser(user) {
  //return this.http.post<any>(this._registerUrl, user);
  //}

  //loginUser(user) {
  //return this.http.post<any>(this._loginUrl, user);
  //}
  viewinsurerPlans(email,inusrance_for){
    return this.http.get<any>(this._viewinsurerplansUrl+'?email='+email+'&insurance_for='+inusrance_for);
  }
  getToken() {
    return sessionStorage.getItem('token');
  }

  removeToken() {
    sessionStorage.removeItem('token');
  }
  getitem(){
    return localStorage.getItem('userData');
  }

  loggedIn() {
    return !!localStorage.getItem('userData');
  }
  currentUserValue(){
    localStorage.getItem('userData');
  }

  allUsers() {
    return this.http.get<any>(this._userUrl);
  }

  getVehicles(vehicleNo) {
    return this.http.get<any>(this._getvehicleUrl + '?vehicleNo=' + vehicleNo);
  }

  insurer(user) {
    return this.http.post<any>(this._insurerUrl, user);
  }
  insurercategory(insurance,email) {
    return this.http.post<any>(this._insurercategoryUrl+'?email='+ email, insurance);
  }
  insureraddplans(insurance) {
    return this.http.post<any>(this._planurl, insurance);
  }
  buyinsurance(insurance) {
    return this.http.post<any>(this._buyplansUrl, insurance);
  }
  viewinsurance(insurance_for) {
    return this.http.get<any>(this._viewplansUrl+'?insurance_for='+insurance_for);
  }
  getpolicies(email) {
    return this.http.get<any>(this._getpoliciesUrl + '?email=' + email);
  }
  claimInsurance(insurance) {
    return this.http.post<any>(this._claimUrl, insurance);
  }
  
  
}
