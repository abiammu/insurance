import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-headers',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  getCookieValue = {};

  /* Role Based Menu Items */
  policyHolder: boolean = false;
  insuranceProvider: boolean = false;
  inspector: boolean = false;
  loginRegister: boolean = false;
  logout: boolean = false;

  constructor(private router: Router, private _auth: AuthService) {}

  ngOnInit() {
    this.getCookieValue = this.getlocalStorage('userData');

    this.initFunction();
  }

  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }

  initFunction() {
      // handling menu items
    if (this.getCookieValue != null) {
      console.log('User has logged in');
      if (this.getCookieValue['role'] == 'insurance_holder') {
        this.policyHolder = true;
      } else if (this.getCookieValue['role'] == 'insurance_provider') {
        this.insuranceProvider = true;
      } else if (this.getCookieValue['role'] == 'inspector') {
        this.inspector = true;
      }
    } else {
      console.log('User is not logged in');
    }

    // handling login logout & register

    if(this.getCookieValue == null){
        this.loginRegister = true;
        console.log('LoginRegister');
    }else if(this.getCookieValue !== null){
        this.logout = true;
        console.log('Logout');
    }
  }

  logoutFunction(){
      /* Removing Cookies */
      localStorage.removeItem('userData');
      localStorage.removeItem('claimInsuranceVehicleNo');
      localStorage.removeItem('insurance_for');
      localStorage.removeItem('planId');      
      
      //this.router.navigateByUrl('/');
  }
}
