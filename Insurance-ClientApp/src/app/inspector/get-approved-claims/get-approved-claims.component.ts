import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert';

@Component({
    selector: 'app-get-approved-claims',
    templateUrl: './get-approved-claims.component.html',
    styleUrls: ['./get-approved-claims.component.css']
})
export class GetApprovedClaimsComponent implements OnInit {
    claimRequestObj = [];
    showTable: boolean = false;

    approvalObj = {
        approve:''
    };

    buttonClickFlag = '';

    constructor(
        private _http: HttpClient
    ) { }

    ngOnInit() {
        this.getApprovedClaims();
    }

    getApprovedClaims() {
        var apiClaimRequest = 'indiuminsurance.ml/api/insurance-portal/inspector-get-approved-claims';
        this._http.get(apiClaimRequest).subscribe(data => {
            console.log('data', data);
            if(data['claimData'].length >0 ){
                this.showTable = true;
            }

            this.claimRequestObj = data['claimData'];
        }, error => {
            console.log('error', error);
        })
    }

}
