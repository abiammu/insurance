import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetApprovedClaimsComponent } from './get-approved-claims.component';

describe('GetApprovedClaimsComponent', () => {
  let component: GetApprovedClaimsComponent;
  let fixture: ComponentFixture<GetApprovedClaimsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetApprovedClaimsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetApprovedClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
