import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inspector-dashboard',
  templateUrl: './inspector-dashboard.component.html',
  styleUrls: ['./inspector-dashboard.component.css']
})
export class InspectorDashboardComponent implements OnInit {
  userEmail = {};

  ngOnInit() {
    // Getting Email
    this.userEmail = this.getlocalStorage('userData')['email'];
  }

  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }
}
