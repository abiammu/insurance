export const navigationItems = [
  {
    name: 'Show Claim Request',
    url: '/claim-request',
    icon: 'fa fa-gear'
  },
  {
    name: 'Approved Claims',
    url: '/approved-claims',
    icon: 'fa fa-check'
  },
  {
    name: 'Rejected Claims',
    url: '/rejected-claims',
    icon: 'fa fa-close'
  }
];
