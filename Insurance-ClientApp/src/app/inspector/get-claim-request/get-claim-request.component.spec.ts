import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetClaimRequestComponent } from './get-claim-request.component';

describe('GetClaimRequestComponent', () => {
  let component: GetClaimRequestComponent;
  let fixture: ComponentFixture<GetClaimRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetClaimRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetClaimRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
