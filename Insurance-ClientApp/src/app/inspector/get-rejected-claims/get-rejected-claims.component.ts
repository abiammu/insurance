import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert';

@Component({
    selector: 'app-get-rejected-claims',
    templateUrl: './get-rejected-claims.component.html',
    styleUrls: ['./get-rejected-claims.component.css']
})
export class GetRejectedClaimsComponent implements OnInit {
    claimRequestObj = [];
    showTable: boolean = false;

    approvalObj = {
        approve:''
    };

    buttonClickFlag = '';

    constructor(
        private _http: HttpClient
    ) { }

    ngOnInit() {
        this.getRejectedClaims();
    }

    getRejectedClaims() {
        var apiClaimRequest = 'indiuminsurance.ml/api/insurance-portal/inspector-get-rejected-claims';
        this._http.get(apiClaimRequest).subscribe(data => {
            console.log('data', data);
            if(data['claimData'].length >0 ){
                this.showTable = true;
            }

            this.claimRequestObj = data['claimData'];
        }, error => {
            console.log('error', error);
        })
    }

}
