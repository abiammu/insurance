import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetRejectedClaimsComponent } from './get-rejected-claims.component';

describe('GetApprovedClaimsComponent', () => {
  let component: GetRejectedClaimsComponent;
  let fixture: ComponentFixture<GetRejectedClaimsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetRejectedClaimsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetRejectedClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
