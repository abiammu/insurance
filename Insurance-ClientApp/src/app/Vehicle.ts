export class Vehicle {

        email: String;
        owner_name: String;
        vehicle_model: String;
        vehicle_number: String;
        vehicle_price: Number;
        purchased_date: Date;
        accident_involved: Boolean;
        comments: String;
        
        

}