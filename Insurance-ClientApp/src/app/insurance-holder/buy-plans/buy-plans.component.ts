import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'app-buy-plans',
    templateUrl: './buy-plans.component.html',
    styleUrls: ['./buy-plans.component.css']
})
export class BuyPlansComponent implements OnInit {
    InsuranceData: any = {};
    accidents = ["yes", "no"];
    hideFormContent = true;
    successMsg = false;
    currentDate: any;
    constructor(private _auth: AuthService, private cookieService: CookieService) { }

    ngOnInit() {
        //Initializing Email Field
        this.InsuranceData.email = this.getlocalStorage('userData').email;
    }
    date(){
        var date = new Date();
      

        
var month = ("0"+(date.getMonth() + 1)).slice(-2); //months from 1-12
var day = ("0"+(date.getDate())).slice(-2);
var year = date.getFullYear();


this.currentDate = (year + "-" + month + "-" + day).toString();
console.log('currentDate', this.currentDate);
console.log('purchased date',this.InsuranceData.purchased_date);
    }

    getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }

    buyplans() {
       
        this.InsuranceData.getPlansId = localStorage.getItem('planId');
        this.InsuranceData.email = this.getlocalStorage('userData').email;

        this._auth.buyinsurance(this.InsuranceData).subscribe(res => {
            localStorage.setItem('vehicleNumber',this.InsuranceData.vehicle_number)
            /* Hiding form content */
            this.hideFormContent = false;
            this.successMsg = true;
            swal({
                text:"Buying insurance plan done successfully",
                icon:"success"
            });
        },
            err => {
                console.log(err)
                if (err.error.message == "Vehicle already insured" && err.error.status == 409) {
                    swal({
                        text:"Vehicle Already Insured",
                        icon:"error"
                    });
                }
                if (err.error.message == "Plans not found to map in Insurance" && err.error.status == 500) {
                    swal({
                        text:"Plans Not Found to Map with Insurance",
                        icon:"error"
                    });
                }
            }
        )
    }
}
