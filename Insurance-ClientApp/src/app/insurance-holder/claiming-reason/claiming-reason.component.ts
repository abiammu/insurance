import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert';

@Component({
    selector: 'app-claiming-reason',
    templateUrl: './claiming-reason.component.html',
    styleUrls: ['./claiming-reason.component.css']
})
export class ClaimingReasonComponent implements OnInit {
    
    @Input() public parentdata; 
    
    claimReasonObj: any = {};
    selectedPath: File = null;
    hideFormContent = true;
    successMsg = false;
    currentDate: any;
    claims=["Lost","Theft","Accident","Fired","Others"];
    public inputdata = this.parentdata;

    constructor(
        private _auth: AuthService,
        private _http: HttpClient
        
    ) { }



    ngOnInit() {
        //Initializing Vehicle Input
        this.claimReasonObj.vehicleNo = localStorage.getItem('claimInsuranceVehicleNo');
    }


    dateCompare(){
        
        var date = new Date();
      

        
var month = ("0"+(date.getMonth() + 1)).slice(-2); //months from 1-12
var day = ("0"+(date.getDate())).slice(-2);
var year = date.getFullYear();


this.currentDate = (year + "-" + month + "-" + day).toString();
console.log('currentDate', this.currentDate);
console.log("purchased date", this.parentdata);
console.log("incident date", this.claimReasonObj.Incident_Date);

        
    }

    getlocalStorage(cookieName) {
        return JSON.parse(localStorage.getItem(cookieName));
    }

    fileSelected(event) {
        this.selectedPath = <File>event.target.files[0];
        console.log('this selected path', this.selectedPath);
    }

    claimInsurance() {

        console.log('this.claimReasonObj ', this.claimReasonObj);
        var dataFromCookie = this.getlocalStorage('userData').email;
        var cookieVehicleNo = localStorage.getItem('claimInsuranceVehicleNo');


        const formData = new FormData();
        formData.append('vehicleNo', cookieVehicleNo);
        formData.append('email', dataFromCookie);
        formData.append('claim_reason', this.claimReasonObj.claim_reason);
        formData.append('Incident_Date',this.claimReasonObj.Incident_Date);
        formData.append('claim_reason_image', this.selectedPath, this.selectedPath.name);

        this._http.post('indiuminsurance.ml/api/insurance-portal/user-claim-insurance', formData)
            .subscribe(
                res => {
                    console.log('Claiming Request Posted', res);
                    
                    //if(res['message']=='Claiming request already sent'&& res['status']==200){
                        //swal({
                            //text:"Claiming request already sent",
                            //icon:"error",   
                        //});
                    //}
                    if(res['message']=='Invalid Date' && res['status']==401){
                        swal({
                            text:"Invalid Date",
                            icon:"error",
                        })
                    }
                 
                    
                    
                    
                    if(res['message']=="Claiming request is sent"){
                        this.hideFormContent = false;
                    this.successMsg = true;
                    swal({
                            text:"Your Claim Request has been posted, Authorities will get in touch with you shortly",
                            icon:"success",
                        });
                    }
                    
                },
                error => {
                    console.log(error);
                    if(error.error.message=='Invalid Date'&& error.error.status==401){
                        swal({
                            text:"Invalid Date",
                            icon:"error",
                        })
                    }
                    
                    
                }
                
            );

    }
}
