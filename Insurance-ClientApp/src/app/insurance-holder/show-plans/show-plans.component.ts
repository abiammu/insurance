import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show-plans',
  templateUrl: './show-plans.component.html',
  styleUrls: ['./show-plans.component.css']
})
export class ShowPlansComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  viewinsurance(insurance_for) {
    localStorage.setItem('insurance_for', insurance_for);
    this.router.navigate(['/view-plans']);
  }
Back(){
  this.router.navigate(['/holder-dashboard']);
}
}
