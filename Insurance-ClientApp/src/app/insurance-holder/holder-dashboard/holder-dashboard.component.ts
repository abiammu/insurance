import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-holder-dashboard',
  templateUrl: './holder-dashboard.component.html',
  styleUrls: ['./holder-dashboard.component.css']
})
export class HolderDashboardComponent implements OnInit {
  userEmail = {};

  constructor() {}

  ngOnInit() {
    // Getting Email
    this.userEmail = this.getlocalStorage('userData')['email'];
  }

  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }
}
