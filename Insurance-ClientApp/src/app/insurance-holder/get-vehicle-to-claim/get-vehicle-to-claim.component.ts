import { Component, OnInit, Input, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EventEmitter } from 'protractor';


@Component({
    selector: 'app-get-vehicle-to-claim',
    templateUrl: './get-vehicle-to-claim.component.html',
    styleUrls: ['./get-vehicle-to-claim.component.css']
})
export class GetVehicleToClaimComponent implements OnInit {


    

    vehicleData = [];
    claimStatusData=[];
    
   

    constructor(
        private _http: HttpClient
    ) { 
        
    }
    ngOnInit() {
        this.getClaimVehicleData();
        
    }

    

    getClaimVehicleData() {
        var getVehicleNo = localStorage.getItem('claimInsuranceVehicleNo');
      
        var apiGetClaimVehicle = 'indiuminsurance.ml/api/insurance-portal/user-claim-insurance-vehicle-data?vehicleNo=' + getVehicleNo


        this._http.get(apiGetClaimVehicle).subscribe(
            data => {
                this.vehicleData = data['vehicle_data'];
                console.log('this.vehicleData', this.vehicleData);
            },
            error => {
                console.log('error', error);
            }
        );
        }


    }

