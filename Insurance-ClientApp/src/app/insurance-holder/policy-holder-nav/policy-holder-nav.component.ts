import { Component, OnInit } from '@angular/core';
import { navigationItems } from './navigation';
import { Router } from '@angular/router';

@Component({
  selector: 'app-policy-holder-nav',
  templateUrl: './policy-holder-nav.component.html',
  styleUrls: ['./policy-holder-nav.component.scss']
})
export class PolicyHolderNavComponent implements OnInit {
  public navItems = [];
  userImage:any;
  getCookieValue = {};
  constructor(
      private router: Router
  ) {
    this.navItems = navigationItems;
  }

  ngOnInit() {
    this.userImage=localStorage.getItem('userImg');
    this.getCookieValue=this.getlocalStorage('userData')['email'];
    console.log('useremail::',this.getCookieValue);
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
}

  logout(){
      /* Removing Cookies */
      localStorage.removeItem('userData');
      localStorage.removeItem('claimInsuranceVehicleNo');
      localStorage.removeItem('insurance_for');
      localStorage.removeItem('planId');
      localStorage.removeItem('userImg');
      
      this.router.navigateByUrl('/');
  }
}
