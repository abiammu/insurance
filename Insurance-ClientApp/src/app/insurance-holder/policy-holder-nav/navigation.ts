export const navigationItems = [
  {
    name: 'View Plans',
    url: '/userhome',
    icon: 'fa fa-pencil'
  },
  {
    name: 'My Policies & Claims',
    url: '/my-policies',
    icon: 'fa fa-paper-plane'
  },
  {
    name: 'View Claim Status',
    url: '/claim-status',
    icon: 'fa fa-bar-chart'
  }
];
