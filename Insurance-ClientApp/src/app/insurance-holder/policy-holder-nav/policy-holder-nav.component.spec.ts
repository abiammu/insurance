import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyHolderNavComponent } from './policy-holder-nav.component';

describe('PolicyHolderNavComponent', () => {
  let component: PolicyHolderNavComponent;
  let fixture: ComponentFixture<PolicyHolderNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyHolderNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyHolderNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
