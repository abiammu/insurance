import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-claim-status',
    templateUrl: './claim-status.component.html',
    styleUrls: ['./claim-status.component.scss']
})
export class ClaimStatusComponent implements OnInit {
    claimStatusObj:any = [];
    getCookieValue = {};
    showData: boolean = false;

    constructor(
        private _http: HttpClient
    ) { }

    ngOnInit() {
        this.getCookieValue = this.getlocalStorage('userData')['email'];

        this.getClaimStatus();
    }

    getlocalStorage(cookieName) {
        return JSON.parse(localStorage.getItem(cookieName));
    }

    getClaimStatus() {
        var apiGetClaimStatus = 'indiuminsurance.ml/api/insurance-portal/user-claim-status?email='+this.getCookieValue;

        this._http.get(apiGetClaimStatus).subscribe(data => {
            this.claimStatusObj = data['vehicle_data'];

            if(this.claimStatusObj.length > 0){
                this.showData = true;
            }

            console.log('this.claimStatusObj ',this.claimStatusObj);
        }, error => {
            console.log('error',error);
        });
    }
}
