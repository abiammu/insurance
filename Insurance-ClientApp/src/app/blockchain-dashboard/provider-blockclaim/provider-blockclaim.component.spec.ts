import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderBlockclaimComponent } from './provider-blockclaim.component';

describe('ProviderBlockclaimComponent', () => {
  let component: ProviderBlockclaimComponent;
  let fixture: ComponentFixture<ProviderBlockclaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderBlockclaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderBlockclaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
