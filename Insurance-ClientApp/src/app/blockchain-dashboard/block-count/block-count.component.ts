import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-block-count',
  templateUrl: './block-count.component.html',
  styleUrls: ['./block-count.component.scss']
})
export class BlockCountComponent implements OnInit {
  blockDetailsObj = [];
  showTable: boolean = false;
  blockNumber = '';
  constructor(private _http: HttpClient) { }

  ngOnInit() {
    this.getblockcount();

  }
  getblockcount() {
    var apigetBlockCountDetails = 'indiuminsurance.ml/api/insurance-portal/get-total-blocks';
    this._http.get(apigetBlockCountDetails).subscribe(data => {
      this.blockNumber = data['blocks'];
      console.log('BLOCKNO', this.blockNumber);
      console.log('blockdata', data);

    },
      error => {
        console.log('error', error);
      })
  }


}
