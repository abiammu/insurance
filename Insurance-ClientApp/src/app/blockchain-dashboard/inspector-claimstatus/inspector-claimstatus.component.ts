import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-inspector-claimstatus',
  templateUrl: './inspector-claimstatus.component.html',
  styleUrls: ['./inspector-claimstatus.component.scss']
})
export class InspectorClaimstatusComponent implements OnInit {
  getCookieValue={};
  inspectorResults:any=[];
  datas:any=[];
  constructor(private _http:HttpClient) { }

  ngOnInit() {
    this.getCookieValue = this.getlocalStorage('userData').email;
    this.getinspectorclaimdetails();
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
 }
getinspectorclaimdetails(){
  var apigetinspectorclaim="indiuminsurance.ml/api/insurance-portal/inspector-claimsBC?email="+this.getCookieValue;
  this._http.get(apigetinspectorclaim).subscribe((data:any) => {
    console.log('data',data);
    for (var i = 0; i < data.payload.length; i++) {
      this.datas = data.payload[i].Record;
      console.log("data",this.datas)
       this.inspectorResults.push(this.datas)
    }

    console.log(this.inspectorResults);
    error=>{
      console.log('error',error)
    }
  });
}
}

