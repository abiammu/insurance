import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-block-data',
  templateUrl: './block-data.component.html',
  styleUrls: ['./block-data.component.scss']
})
export class BlockDataComponent implements OnInit {
  blockUser='';
  blockcertificate='';
  blocksignin='';
  email = {};
  constructor(private _http:HttpClient) { }

  ngOnInit() {
    this.getblockdata();
    this.email = this.getlocalStorage('userData')['email'];
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
}
  getblockdata()
  {
    this.email = this.getlocalStorage('userData')['email'];
    console.log('getCookieValue', this.email);
    var apigetBlockCertificate = 'indiuminsurance.ml/api/insurance-portal/get-user'+'?email='+this.email;
    this._http.get(apigetBlockCertificate, this.email).subscribe(data => {
      console.log(data);
        this.blockUser = data['user'];
        this.blockcertificate=data['certificate'];
        this.blocksignin=data['signin_certificate'];
        console.log(this.blockUser)
       
    }, error => {
        console.log('error', error);
    })
}
  }
