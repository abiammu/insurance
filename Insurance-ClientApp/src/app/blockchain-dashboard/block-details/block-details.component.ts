import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-block-details',
  templateUrl: './block-details.component.html',
  styleUrls: ['./block-details.component.scss']
})
export class BlockDetailsComponent implements OnInit {
  blockDetailsObj = [];
  showTable: boolean = false;
  constructor(private _http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.getblockdetails();  
  }
  getblockdetails() {
    var apigetBlockDetails = 'indiuminsurance.ml/api/insurance-portal/get-block-details';
    this._http.get(apigetBlockDetails).subscribe(data => {
        this.blockDetailsObj = data['blockData'];
    }, error => {
        console.log('error', error);
    })
    }
}





