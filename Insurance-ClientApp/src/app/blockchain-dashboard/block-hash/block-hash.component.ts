import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-block-hash',
  templateUrl: './block-hash.component.html',
  styleUrls: ['./block-hash.component.scss']
})
export class BlockHashComponent implements OnInit {
  blockHashesObj = [];
  showTable: boolean = false;
  constructor(private _http:HttpClient) { }

  ngOnInit() {
    this.gethashdetails();
  }
  gethashdetails() {
    var apigethashDetails = 'indiuminsurance.ml/api/insurance-portal/get-block-hash';
    this._http.get(apigethashDetails).subscribe(data => {
        console.log('blockdata', data);
        console.log('sdsadsa', data['blockData']);
        if(data['blockData'].length >0 ){
            this.showTable = true;
        }

        this.blockHashesObj = data['blockData'];
        
    }, error => {
        console.log('error', error);
    })
}
}
