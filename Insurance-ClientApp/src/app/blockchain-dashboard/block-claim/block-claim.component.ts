import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResolveEnd } from '@angular/router';
import { resolve } from 'dns';

@Component({
  selector: 'app-block-claim',
  templateUrl: './block-claim.component.html',
  styleUrls: ['./block-claim.component.scss']
})
export class BlockClaimComponent implements OnInit {
  showData: boolean = false;
  show: boolean = false;

  blockemail = '';
  blockincident = '';
  blockstatus = '';
  blockreason = '';
  blockvehicle = '';
  blockinitiate = '';
  blockdata = '';
  blockprovider = '';
  datas: any = [];
  Blockdata: any = {};
  getCookieValue = {};
  claim_id = {};
  claimstatusObj = {};
  allResults: any;

  constructor(private _http: HttpClient) { }

  ngOnInit() {
    this.allResults = [];
    this.getCookieValue = this.getlocalStorage('userData')['email'];
    //this.getblockclaimdetails(this.Blockdata);

    var btnElement = document.getElementById('bc_submitBtn');

    btnElement.onclick = () => {
      console.log('this.Blockdata.vehicle_number:', this.Blockdata.vehicle_number);
      if (this.Blockdata.vehicle_number != '' && this.Blockdata.vehicle_number != undefined) {
        this.getblockclaimdetails(this.Blockdata);
        console.log('Vehicle No.');
      } else{
        this.getblockvehicledetails();
        console.log('Email Query');

      }
    }

  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }

  getblockclaimdetails(Blockdata) {
    this.claim_id = this.getCookieValue + Blockdata.vehicle_number;
    console.log(this.claim_id);
    var apigetBlockDetails = 'indiuminsurance.ml/api/insurance-portal/user-claim-statusBC?claim_id=' + this.claim_id;
    this._http.get(apigetBlockDetails).subscribe((data: any) => {
      console.log(data);
      this.show = false;
      this.showData = true;

      this.blockemail = data.payload.email;
      this.blockvehicle = data.payload.vehicleNo;
      this.blockincident = data.payload.Incident_Date;
      this.blockstatus = data.payload.claim_status;
      this.blockreason = data.payload.claim_reason;
      this.blockinitiate = data.payload.claim_initiated;


      console.log(this.blockemail)
      
    }, error => {
      console.log('error', error);
      
    });
  }

  getblockvehicledetails() {
    this.allResults = [];
    if(this.Blockdata.email==this.getCookieValue){
    var apigetBlockvehicleDetails = 'indiuminsurance.ml/api/insurance-portal/get-user-vehicle?email=' + this.Blockdata.email;
    this._http.get(apigetBlockvehicleDetails).subscribe((data: any) => {
      this.showData = false;
      this.show = true;
      for (var i = 0; i < data.payload.length; i++) {
        console.log("accident_involved",data.payload[i].Record.Incident_Date)
        this.datas = data.payload[i].Record;
        this.allResults.push(this.datas)
      }

      console.log(this.allResults);
      error=>{
        console.log('error',error)
      }
    });
  }else{
    swal({
      text:"Enter valid Email",
      icon:"error"
    })
  }
  }
}

