import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    rolePolicyHolder: boolean ;
    roleInsuranceProvider: boolean;
    roleInspector: boolean;
    role = '';
    selectedPath: File = null;
    hideForm: boolean;
    successMsg: boolean;
    /* Insurance Holder */
    apiIHSignup =
        'indiuminsurance.ml/api/insurance-portal/policy-holder-signup';

        insurance_holder: any;
        
    /* Insurance Provider */
    apiIPSignup =
        'indiuminsurance.ml/api/insurance-portal/register-insurance-provider';

        insurance_provider: any;

    /* Insurance Provider */
    apiInspectorSignup =
        'indiuminsurance.ml/api/insurance-portal/inspector-signup';

        inspector: any;

    constructor(private _http: HttpClient) { }  

    ngOnInit() {
    
        this.rolePolicyHolder = false;
        this.roleInsuranceProvider = false;
        this.roleInspector = false;
        this.hideForm = true;
        this.successMsg = false;


        this.insurance_holder = {
            role: '',
            firstname: '',
        };

        this.insurance_provider = {
            role: '',
            orgName: ''
        };

        this.inspector = {
            role: '',
            name: ''
        };

     }
     
     fileSelected(event) {
        this.selectedPath = <File>event.target.files[0];
        console.log('this selected path', this.selectedPath);
    }

    getRole() {
        this.successMsg = false;
        if (this.role == 'insurance_holder') {
            this.rolePolicyHolder = true;
        } else {
            this.rolePolicyHolder = false;
        }
        if (this.role == 'insurance_provider') {
            this.roleInsuranceProvider = true;
        } else {
            this.roleInsuranceProvider = false;
        }
        if (this.role == 'inspector') {
            this.roleInspector = true;
        } else {
            this.roleInspector = false;
        }

        /* Setting Role to Insurance Holder */
        this.insurance_holder.role = this.role;

        /* Setting Role to Insurance Provider */
        this.insurance_provider.role = this.role;

        /* Setting Role to Inspector */
        this.inspector.role = this.role;
    }


    backToRegisterpage(){
        this.hideForm = true;
        this.successMsg = false;
        

    }

    /* Insurance Holder */
    insuranceHolder() {
        const formData = new FormData();
        formData.append('role', this.role);
        formData.append('email', this.insurance_holder.email);
        formData.append('firstname', this.insurance_holder.firstname);
        formData.append('lastname', this.insurance_holder.lastname);
        formData.append('password', this.insurance_holder.password);
        formData.append('image', this.selectedPath, this.selectedPath.name);
        this._http.post(this.apiIHSignup, formData).subscribe(
            data => {
                console.log(data);
                /* Hiding form content */
                this.hideForm = false;
                this.successMsg = true;
                this.rolePolicyHolder = false;
                this.roleInsuranceProvider = false;
                this.roleInspector = false;
                console.log('Insurer Signup Successful', data);
                swal(
                    'Registration Success',
                    `Hi ${this.insurance_holder.firstname} registration successful!`
                );
                this.insurance_holder = {};
                this.insurance_provider = {};
                this.inspector = {};
                
            },
            error => {
                console.log('failed to signup', error.status);
                if (error.status == 409) {
                    swal('User Already Registered With The Given Mail Address');

                } else {
                     swal('Failed To Signup User');
                 }
            }
        );
    }

    /* Insurance Provider */
    insuranceProvider() {
        const formData = new FormData();
        formData.append('role', this.role);
        formData.append('orgName', this.insurance_provider.orgName);
        formData.append('email', this.insurance_provider.email);
        formData.append('contact', this.insurance_provider.contact);
        formData.append('password', this.insurance_provider.password);
        formData.append('image', this.selectedPath, this.selectedPath.name);
        this._http.post(this.apiIPSignup, formData).subscribe(
            data => {
                /* Hiding form content */
                this.hideForm = false;
                this.successMsg = true;
                this.rolePolicyHolder = false;
                this.roleInsuranceProvider = false;
                this.roleInspector = false;
                console.log('Insurance Provider Signup Successful', data);
                swal(
                    'Registration Success',
                    `Hi ${this.insurance_provider.orgName} registration successful!`
                );

                this.insurance_holder = {};
                this.insurance_provider = {};
                this.inspector = {};
            },
            error => {
                // this.hideForm = false;
                console.log('failed to signup', error);
                if (error.status == 409) {
                    swal('User Already Registered With The Given Mail Address');
                } else {
                    swal('Failed To Signup User');
                }
            }
        );
    }

    /* Inspector */
    inspectorFunc() {
        const formData = new FormData();
        formData.append('role', this.role);
        formData.append('email', this.inspector.email);
        formData.append('name', this.inspector.name);
        formData.append('contact', this.inspector.contact);
        formData.append('password', this.inspector.password);
        formData.append('image', this.selectedPath, this.selectedPath.name);
        this._http.post(this.apiInspectorSignup,formData).subscribe(
            data => {
                /* Hiding form content */

                this.hideForm = false;
                this.successMsg = true;
                this.rolePolicyHolder = false;
                this.roleInsuranceProvider = false;
                this.roleInspector = false;
                console.log('Signed up as inspector', data);
                
                swal(
                    'Registration Success',
                    `Hi ${this.inspector.name} registration successful!`
                );

                this.insurance_holder = {};
                this.insurance_provider = {};
                this.inspector = {};
            },
            error => {
                console.log('failed to signup', error);
                // this.hideForm = false;
                if (error.status == 409) {
                    swal('User Already Registered With The Given Mail Address');
                } else {
                    swal('Failed To Signup User');
                }
            }
        );
    }
}