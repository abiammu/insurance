import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from 'src/app/auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public signinObj: any = {};
  userObj = {};
  blockimage = '';
  modalRef: BsModalRef;
  public inputData: any = {};
  otpIsVerified: any;
  isVerified: any = '';
  verified: any;
  public loading: boolean;
  defaultImg: any;
  userImage: any;

  apiLogin = 'indiuminsurance.ml/api/insurance-portal/user-signin';


  @ViewChild('dummy') otpModal: TemplateRef<any>;

  constructor(
    private http: HttpClient,

    private _router: Router,
    public modalService: BsModalService,

  ) { }



  ngOnInit() {
    /* Removing Cookies */
    localStorage.removeItem('userData');
    localStorage.removeItem('claimInsuranceVehicleNo');
    localStorage.removeItem('insurance_for');
    localStorage.removeItem('planId');
    this.signinObj = {
      email: '',
      password: '',
      otp: '',
    }

    console.log('UserImage::', this.userImage);

    // Default Image
    this.defaultImg = './assets/images/avatar1.png';
  }


  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }
  // openModal(element: ElementRef<any>) {
  //    this.modalRef = this.modalService.show(element);
  //  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    console.log('this.modalRef:', typeof this.modalRef.hide)
  }
  closeModal() {
    if (typeof this.modalRef !== 'undefined') {
      this.modalRef.hide();
    }else{
      console.log('modalRef is not initialized.');
    }
  }

  otpverification() {
    var apiOtp = 'indiuminsurance.ml/api/insurance-portal/verifyOtp?email=' + this.signinObj.email;
    this.http.post(apiOtp, this.signinObj).subscribe(
      data => {
        console.log(data);
        this.userSignin();
      },
      error => {
        if (error.status == 409) {
          swal({
            text: "Incorrect OTP",
            icon: "error",
          });
        }

      }
    );

  }

  isUserVerified() {
    var apiUserVerified = 'indiuminsurance.ml/api/insurance-portal/isUserVerified?email=' + this.signinObj.email;
    this.http.get(apiUserVerified).subscribe(
      data => {
        this.otpIsVerified = data;

        this.isVerified = this.otpIsVerified.data.isVerified;

        console.log('Is Verified Func::', this.isVerified);
      }
    )
  }
  upload() {
    var uploadimage = 'indiuminsurance.ml/api/insurance-portal/user-image?email=' + this.signinObj.email;

    this.http.get(uploadimage).subscribe(
      (data) => {
        console.log('Data::', data);
        //this.blockimage = data.image;
        //console.log("image",this.blockimage);
        this.userImage = data;
        this.userImage = 'indiuminsurance.ml/' + this.userImage.userData.image;
        this.userImage = this.userImage.replace(/\/public/, '');

        console.log('userImg::', this.userImage);
        localStorage.setItem('userImg', this.userImage);
      }
    )
  }

  userSignin() {

    this.http.post(this.apiLogin, this.signinObj).subscribe(
      data => {

        this.isUserVerified();

        // res=>{
        // Save data in Session Storage
        this.userObj = {
          email: data['userData']['email'],
          role: data['userData']['role'],
          orgName: data['userData']['orgName']
        };
        localStorage.setItem('userData', JSON.stringify(this.userObj));

        if (data['userData']['role'] == 'insurance_holder') {
          //location.href = '/holder-dashboard';
          swal({

            text: "Insurance Holder login successfully",
            icon: "success",
          });

          this._router.navigateByUrl('/holder-dashboard');
          this.closeModal();
        } else if (data['userData']['role'] == 'insurance_provider') {
          //location.href = '/provider-dashboard';
          swal({

            text: "Insurance Provider login successfully",
            icon: "success",
          });
          this._router.navigateByUrl('/provider-dashboard');
          this.closeModal();
        } else if (data['userData']['role'] == 'inspector') {
          //location.href = '/inspector-dashboard';
          swal({

            text: "Inspector login successfully",
            icon: "success",
          });
          this._router.navigateByUrl('/inspector-dashboard');
          this.closeModal();


        }
      },
      error => {
        console.log('UI:', error)
        if (error.status == 402) {
          this.openModal(this.otpModal);
        }
        if (error.status == 404) {
          swal({

            text: "User not found check the mail address",
            icon: "error",
          });
        }
        if (error.status == 401) {
          swal({

            text: "Incorrect Password",
            icon: "error",
          });
        }

      }
    );
  }
}
