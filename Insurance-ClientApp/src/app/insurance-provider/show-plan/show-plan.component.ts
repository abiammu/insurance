import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-show-plan',
  templateUrl: './show-plan.component.html',
  styleUrls: ['./show-plan.component.scss']
})
export class ShowPlanComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  viewinsurance(insurance_for) {
    localStorage.setItem('insurance_for', insurance_for);
    this.router.navigate(['/view-plan']);
  }
Back(){
  this.router.navigate(['/provider-dashboard']);
}
}
