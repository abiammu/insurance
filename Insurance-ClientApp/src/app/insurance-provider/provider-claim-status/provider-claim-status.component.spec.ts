import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderClaimStatusComponent } from './provider-claim-status.component';

describe('ProviderClaimStatusComponent', () => {
  let component: ProviderClaimStatusComponent;
  let fixture: ComponentFixture<ProviderClaimStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderClaimStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderClaimStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
