import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-provider-claim-status',
    templateUrl: './provider-claim-status.component.html',
    styleUrls: ['./provider-claim-status.component.scss']
})
export class ProviderClaimStatusComponent implements OnInit {
    claimStatusObj:any = [];
    getCookieValue = {};
    getemail={};
    showData: boolean = false;

    constructor(
        private _http: HttpClient
    ) { }

    ngOnInit() {
        this.getCookieValue = this.getlocalStorage('userData')['email'];
        
        this.getClaimStatus();
    }

    getlocalStorage(cookieName) {
        return JSON.parse(localStorage.getItem(cookieName));
    }

    getClaimStatus() {
        this.getCookieValue = this.getlocalStorage('userData')['email'];

        var apiGetClaimStatus = 'indiuminsurance.ml/api/insurance-portal/user-claims'+'?email='+this.getCookieValue;

        this._http.get(apiGetClaimStatus).subscribe(data => {
            this.claimStatusObj = data['vehicle_data'];

            if(this.claimStatusObj.length > 0){
                this.showData = true;
            }

            console.log('this.claimStatusObj ',this.claimStatusObj);
        }, error => {
            console.log('error',error);
        });
    }
}

