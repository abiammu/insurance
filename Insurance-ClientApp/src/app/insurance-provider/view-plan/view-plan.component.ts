import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { Insurance } from 'src/app/Insurance';
import { Router } from '@angular/router';
@Component({
    selector: 'app-view-plan',
    templateUrl: './view-plan.component.html',
    styleUrls: ['./view-plan.component.css']
})
export class ViewPlanComponent implements OnInit {
    public insurances: Insurance[];
    getInsuranceForVal = '';
    getUserData = {};
    getemail={};
    showDataFlag: boolean = false;
    constructor(private _auth: AuthService, private router: Router) { }

    ngOnInit() {
        this.viewinsurerPlans();  
        
    }

    getlocalStorage(cookieName) {
        return JSON.parse(localStorage.getItem(cookieName));
    }


    viewinsurerPlans() {
        var getInsuranceFor = localStorage.getItem('insurance_for');
        /* getting user data from cookie */
        this.getUserData = this.getlocalStorage('userData').role;
        this.getemail=this.getlocalStorage('userData').email;
        
        console.log('getInsuranceFor_localstorage',getInsuranceFor)
        this._auth.viewinsurerPlans(this.getemail,getInsuranceFor).subscribe((res: Insurance[]) => {
            console.log('plan data:', res);
            if(res['payload'].length > 0){
                this.showDataFlag = true;
            }
    
            this.insurances = res;
            console.log(this.insurances);
        });
    }

}
