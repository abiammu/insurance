import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceProviderNavComponent } from './insurance-provider-nav.component';

describe('InsuranceProviderNavComponent', () => {
  let component: InsuranceProviderNavComponent;
  let fixture: ComponentFixture<InsuranceProviderNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuranceProviderNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceProviderNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
