export const navigationItems = [
  {
    name: 'Show Plan',
    url: '/show-plan',
    icon: 'fa fa-gear'
  },
  
  {
    name: 'Add Category',
    url: '/add-category',
    icon: 'fa fa-plus-square'
  
  },
  {
    name: 'Add Plans',
    url: '/add-plans',
    icon: 'fa fa-send'
  },
  {
    name: 'View Claims Status',
    url: '/provider-claim-status',
    icon: 'fa fa-bar-chart'
  }
 
];
