import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-provider-dashboard',
  templateUrl: './provider-dashboard.component.html',
  styleUrls: ['./provider-dashboard.component.css']
})
export class ProviderDashboardComponent implements OnInit {

  userEmail = {};

  ngOnInit() {
    // Getting Email
    this.userEmail = this.getlocalStorage('userData')['email'];
  }

  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }

}
