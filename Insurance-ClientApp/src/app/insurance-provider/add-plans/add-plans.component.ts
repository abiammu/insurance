import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-add-plans',
  templateUrl: './add-plans.component.html',
  styleUrls: ['./add-plans.component.css']
})
export class AddPlansComponent implements OnInit {
  InsuranceData: any = {};
  getCookieValue = {};
  plans=["2 Wheelers","4 Wheelers","Health"];
  years=["1 year","2 years","3 years","4 years","5 years"];
  categoryValues:any;
  constructor(private router: Router, private _auth: AuthService,private http:HttpClient) {}

  ngOnInit() {
    this.getCookieValue = this.getlocalStorage('userData')['email'];
    this.getcategory();
    this.InsuranceData={
      category:'',
    }
    this.InsuranceData.insurance_provider_name=this.getlocalStorage('userData')['orgName'];
    
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
}
  getcategory(){
    console.log('email',this.getCookieValue)
    var apigetcategory="indiuminsurance.ml/api/insurance-portal/insurer-getCategory?email="+this.getCookieValue;
    this.http.get(apigetcategory).subscribe(data =>{
      this.categoryValues=data['payload'];
      console.log('data',this.categoryValues);
    });

  }
  insureraddplans() {
    this.InsuranceData.insurance_provider_name=this.getlocalStorage('userData')['orgName'];
    console.log('InsurerData', this.InsuranceData);
      this._auth.insureraddplans(this.InsuranceData).subscribe( res => {
      console.log('res', res);
      swal({
              
        text: "Insurance Plans Added Successfully",
        icon: "success",
      });
      },
      err => {
          console.log(err)
          if (err.error.message =="Plans Already Added !" && err.error.status == 409) {
            swal({
              
              text: "Plan Already Exists",
              icon: "error",
            });
          } 
          if (err.error.message =="Category not found to map in Plans" && err.error.status == 500) {
            swal({
              
              text: "Category Not Found",
              icon: "error",
            });
          }

         
          
      }
      )
            
  }
}
