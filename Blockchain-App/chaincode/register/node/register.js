/*
# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
*/

'use strict';
const shim = require('fabric-shim');
const util = require('util');

let Chaincode = class {

    // The Init method is called when the Smart Contract 'register' is instantiated by the blockchain network
    // Best practice is to have any Ledger initialization in separate function -- see initLedger()
    async Init(stub) {
        console.info('=========== Instantiated register chaincode ===========');
        return shim.success();
    }

    // The Invoke method is called as a result of an application request to run the Smart Contract
    // 'register'. The calling application program has also specified the particular smart contract
    // function to be called, with arguments
    async Invoke(stub) {
        let ret = stub.getFunctionAndParameters();
        console.info(ret);

        let method = this[ret.fcn];
        if (!method) {
            console.error('no function of name:' + ret.fcn + ' found');
            throw new Error('Received unknown function ' + ret.fcn + ' invocation');
        }
        try {
            let payload = await method(stub, ret.params,this);
            return shim.success(payload);
        } catch (err) {
            console.log(err);
            return shim.error(err);
        }
    }
    // :::::::::::::::::: Query Vehicles ::::::::::::::::::::::::::::::::::::::::::::

    async queryVehicle(stub, args, res) {
        if (args.length != 1) {
            throw new Error('Incorrect number of arguments. Expecting userName ex:something@Example.com');
        }
        let vehicle_number = args[0];

        let vehicleAsBytes = await stub.getState(vehicle_number); //get the user from chaincode state
        if (!vehicleAsBytes || vehicleAsBytes.toString().length <= 0) {
            throw new Error(vehicle_number + ' does not exist: ');
        }
        console.log(vehicleAsBytes.toString());

        return vehicleAsBytes;
    }

    async initLedger(stub, args) {
    }

    //::::::::::::::::::: USERSIGN UP ::::::::::::::::::::::::

    async userSignup(stub, args) {
        console.info('============= START : add User ===========');
      
        var userSignup = {
            email:args[0],
            firstname: args[1],
            lastname: args[2],
            role: args[3],
            password: args[4],
            image:args[5]
        };

        await stub.putState(args[0], Buffer.from(JSON.stringify(userSignup)));
        console.info('============= END : Create user ===========');
    }

    async claimDetails(stub,args){
        console.info('============= START : Claim Details of user ===========');
    
        var claimdetails={
                    vehicleNo: args[0],
                    email: args[1],
                    claim_initiated: args[2],
                    Incident_Date:args[3],
                    claim_reason: args[4],
                    claim_reason_image: args[5],
                    claim_status: args[6],
                    user_profile:args[7],
                    provider_name: args[8],
                    claim_id:args[9],
                    reason_for_rejection:args[10]
           
        };

        await stub.putState(args[9], Buffer.from(JSON.stringify(claimdetails)));
        console.info('============= END : Claim Details of user ===========');
    }

    async getuserClaims(stub,args){
        console.info('============= START : Book Details ===========');
       
        if (args.length != 1) {
            throw new Error('Incorrect number of arguments. Expecting userName ex:something@Example.com');
        }
        let claim_id = args[0];

        let claim_idAsBytes = await stub.getState(claim_id); //get the user from chaincode state
        if (!claim_idAsBytes || claim_idAsBytes.toString().length <= 0) {
            throw new Error(claim_id + ' does not exist: ');
        }
        console.log(claim_idAsBytes.toString());

        return claim_idAsBytes;
      }
    //:::::::::::::::::::::::::::::::::::::InsurerSignup:::::::::::::::::::::::::::::::
    async insurerSignup(stub, args) {
        console.info('============= START : add User ===========');
        if (args.length != 6) {
            throw new Error('Incorrect number of arguments. Expecting 5');
        }

        var insurerSignup = {
            orgName: args[0],

            email: args[1],
            password: args[2],
            contact: args[3],
            role: args[4],
            image :args[5]
        };

        await stub.putState(args[1], Buffer.from(JSON.stringify(insurerSignup)));
        console.info('============= END : Create user ===========');
    }

    async inspectorSignup(stub, args) {
        console.info('============= START : add User ===========');
        if (args.length != 6) {
            throw new Error('Incorrect number of arguments. Expecting 5');
        }
        var inspectorSignup = {
            name: args[0],
            email: args[1],
            password: args[2],
            contact: args[3],
            role: args[4],
            image: args[5]
        };

        await stub.putState(args[1], Buffer.from(JSON.stringify(inspectorSignup)));
        console.info('============= END : Create user ===========');
    }
    async queryVehicleByEmail(stub, args, thisClass) {
        //   0
        // 'bob'
        if (args.length < 1) {
          throw new Error('Incorrect number of arguments. Expecting owner name.')
        }
        let email = args[0];
        let queryString = {};
        queryString.selector = {};
        queryString.selector.email = email;
        queryString.selector.email = email;
        let method = thisClass['getQueryResultForQueryString'];
        console.log(method+'blockchain')
        let queryResults = await method(stub, JSON.stringify(queryString), thisClass);
        return queryResults; //shim.success(queryResults);
      }

      async getQueryResultForQueryString(stub, queryString, thisClass) {
        console.info('- getQueryResultForQueryString queryString:\n' + queryString)
        let resultsIterator = await stub.getQueryResult(queryString);
        let method = thisClass['getAllResults'];
    
        let results = await method(resultsIterator, false);
    
        return Buffer.from(JSON.stringify(results));
      }

      async getAllResults(iterator, isHistory) {
        let allResults = [];
        while (true) {
          let res = await iterator.next();
    
          if (res.value && res.value.value.toString()) {
            let jsonRes = {};
            console.log(res.value.value.toString('utf8'));
    
            if (isHistory && isHistory === true) {
              jsonRes.TxId = res.value.tx_id;
              jsonRes.Timestamp = res.value.timestamp;
              jsonRes.IsDelete = res.value.is_delete.toString();
              try {
                jsonRes.Value = JSON.parse(res.value.value.toString('utf8'));
              } catch (err) {
                console.log(err);
                jsonRes.Value = res.value.value.toString('utf8');
              }
            } else {
              jsonRes.Key = res.value.key;
              try {
                jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
              } catch (err) {
                console.log(err);
                jsonRes.Record = res.value.value.toString('utf8');
              }
            }
            allResults.push(jsonRes);
          }
          if (res.done) {
            console.log('end of data'+allResults);
            await iterator.close();
            console.info(allResults);
            return allResults;
          }
        }
      }

    // ::::::::::::::::::::::: Insure Vehicle ::::::::::::::::::::::::::::::::::::

    async insure(stub, args) {
        console.info('============= START : add User ===========');
        if (args.length != 8) {
            throw new Error('Incorrect number of arguments. Expecting 8');
        }
        var insure = {
            email: args[0],
            owner_name: args[1],
            vehicle_model: args[2],
            vehicle_number: args[3],
            vehicle_price: args[4],
            purchased_date: args[5],
            accident_involved: args[6],
            getPlansId: args[7]
        };

        await stub.putState(args[3], Buffer.from(JSON.stringify(insure)));
        let email = 'email'
    let emailIndexKey = await stub.createCompositeKey(email, [insure.email]);
    console.info(emailIndexKey);
    await stub.putState(emailIndexKey, Buffer.from('\u0000'));
        console.info('============= END : Create user ===========');
    }

    
    async approval(stub, args) {
        console.info('============= START : change employee age ===========');
        if (args.length != 3) {
          throw new Error('Incorrect number of arguments. Expecting 3');
        }
    
        let claimdetailsAsBytes = await stub.getState(args[0]);
        let claimdetails = JSON.parse(claimdetailsAsBytes);
        claimdetails.claim_status = args[1];
        claimdetails.reason_for_rejection=args[2];    
        await stub.putState(args[0], Buffer.from(JSON.stringify(claimdetails)));
        console.info('============= END : change employee age ===========');
      }

    async queryAllUsers(stub, args) {

        let startKey = '';
        let endKey = '';

        let iterator = await stub.getStateByRange(startKey, endKey);

        let allResults = [];
        while (true) {
            let res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                console.log(res.value.value.toString('utf8'));

                jsonRes.Key = res.value.key;
                try {
                    jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    console.log(err);
                    jsonRes.Record = res.value.value.toString('utf8');
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return Buffer.from(JSON.stringify(allResults));
            }
        }
    }

    async queryAllemail(stub, args) {

      let provider_name =args[0];
      let startKey = '';
      let endKey = '';
      
      let iterator = await stub.getStateByRange(startKey, endKey);

      let allResults = [];
     
      while (true) {
          let res = await iterator.next();

          if (res.value && res.value.value.toString()) {
              let jsonRes = {};
              console.log(res.value.value.toString('utf8'));

              jsonRes.Key = res.value.key;
              console.log(jsonRes.key);
              try {
                  jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
              } catch (err) {
                  console.log(err);
                  jsonRes.Record = res.value.value.toString('utf8');
              }
                console.log(jsonRes.Record.contact)
                if(jsonRes.Record.provider_name == provider_name){
                
              allResults.push(jsonRes);
            }
          }
          if (res.done) {
              console.log('end of data');
              await iterator.close();
              console.info(allResults);
              return Buffer.from(JSON.stringify(allResults));
          }
      }
  }

  async queryAllClaims(stub, args) {

    let startKey = '';
    let endKey = '';
    
    let iterator = await stub.getStateByRange(startKey, endKey);

    let allResults = [];
   
    while (true) {
        let res = await iterator.next();

        if (res.value && res.value.value.toString()) {
            let jsonRes = {};
            console.log(res.value.value.toString('utf8'));

            jsonRes.Key = res.value.key;
            console.log(jsonRes.key);
            try {
                jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
            } catch (err) {
                console.log(err);
                jsonRes.Record = res.value.value.toString('utf8');
            }
              console.log(jsonRes.Record.claim_status )
              if(jsonRes.Record.claim_status && jsonRes.Record.claim_id){
              
            allResults.push(jsonRes);
          }
        }
        if (res.done) {
            console.log('end of data');
            await iterator.close();
            console.info(allResults);
            return Buffer.from(JSON.stringify(allResults));
        }
    }
}

};

shim.start(new Chaincode());