const mongoose = require('mongoose');
const insuredItem = mongoose.model('UserInsure');

exports.getInsuredItem = (req, res, next) => {
    var email = req.query.email;

    insuredItem
        .find({ email: email })
        .populate({
            path: 'insurance_plans',
            populate: { path: 'insurance_category' }
        })
        .exec()
        .then(data => {
            res.status(200).json({
                insuredData: data
            });
        })
        .catch(err => {
            console.log(err);
            res.status().json({
                message: 'Not insured'
            });
        });
};
