const mongoose = require('mongoose');
const insuredItem = mongoose.model('UserInsure');
const ClaimSchema = require('../../models/user-claim-insurance-status');

//chaincode invoke
var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var os = require('os');

exports.getClaimInsuranceVehicleData = (req, res, next) => {
    var vehicleNo = req.query.vehicleNo;
    insuredItem
        .find({ vehicle_number: vehicleNo })
        .populate({ path: 'insurance_plans' })
        .exec()
        .then(vehicleData => {
            res.status(200).json({
                message: 'Vehicle Data !',
                vehicle_data: vehicleData
            });
        })
        .catch(error => {
            res.status(400).json({
                message: 'Vehicle Number Not Found!',
                error: error
            });
        });
};
exports.getClaimInsurancedata = (req, res, next) => {
    var vehicleNo = req.query.vehicleNo;
    ClaimSchema
        .find({ vehicleNo: vehicleNo })
        .populate({ path: 'user_profile' })
        .exec()
        .then(claimStatusData => {

            if(claimStatusData[0].claim_status=="rejected"){
            
            console.log(claimStatusData[0].claim_status);    
                res.status(400).json({
                    message: 'Vehicle Number Not Found!',
                    error: error,
                    status:400,
                });
            }else{
            console.log(claimStatusData[0].claim_status);  
            res.status(200).json({
                message: 'Vehicle Data !',
                claim_data: claimStatusData[0].vehicleNo
            });}
        })
        .catch(error => {
            res.status(400).json({
                message: 'Vehicle Number Not Found!',
                error: error,
                status:400,
            });
        });
};


exports.claimInsurance = (req, res, next) => {
    var vehicleNo = req.body.vehicleNo,
        userEmail = req.body.email,
        claim_initiated = 'yes',
        Incident_Date = req.body.Incident_Date,
        claim_reason = req.body.claim_reason,
        claim_reason_image = req.file.path,
        claim_status = 'pending';
    var reason_for_rejection = null;


    insuredItem
        .find({ vehicle_number: vehicleNo })
        .populate({
            path: 'insurance_plans',
            populate: { path: 'insurance_category' }
        })
        .exec()
        .then(vehicleData => {

            var providerName = vehicleData[0].insurance_plans.insurance_category.insurance_provider_name;
            var purchased_date =Date.parse(vehicleData[0].purchased_date);
            var user_profile = vehicleData[0]._id;
            if (vehicleData.length >= 0) {
                const claimInsuranceObj = new ClaimSchema({
                    _id: new mongoose.Types.ObjectId(),
                    vehicleNo: vehicleNo,
                    email: userEmail,
                    claim_initiated: 'yes',
                    Incident_Date: req.body.Incident_Date,
                    claim_reason: req.body.claim_reason,
                    claim_reason_image: req.file.path,
                    claim_status: 'pending',
                    user_profile: vehicleData[0]._id,
                    provider_name: providerName
                });

                ClaimSchema.find({ vehicleNo: vehicleNo })
                    .exec()
                    .then(findVehicle => {
                        if (findVehicle.length > 0) {
                            if (findVehicle[0].claim_status == 'rejected') {
                                console.log(findVehicle[0].claim_status)
                                
                                var update = {
                                    $set: {
                                        claim_initiated: 'yes',
                                        Incident_Date: req.body.Incident_Date,
                                        claim_reason: req.body.claim_reason,
                                        claim_reason_image: req.file.path,
                                        claim_status: 'pending',
                                        user_profile: vehicleData[0]._id,
                                        provider_name: providerName
                                    }
                                };
                                if(purchased_date < Date.parse(Incident_Date) && Date.parse(Incident_Date) <= Date.now()){
                                ClaimSchema.updateOne({ vehicleNo: vehicleNo }, update)
                                    .then((data) => {
                                        console.log('data')
                                        console.log(data)
                                        var user_profiles = vehicleData[0].insurance_plans;

                                        // :::::::::::::::::: Blockchain part starts here:::::::::::::::::::::::::::::::::

                                        var fabric_client = new Fabric_Client();
                                        // setup the fabric network
                                        var channel = fabric_client.newChannel('mychannel');
                                        var peer = fabric_client.newPeer('grpc://localhost:7051');
                                        channel.addPeer(peer);
                                        var order = fabric_client.newOrderer('grpc://localhost:7050')
                                        channel.addOrderer(order);
                                        //
                                        var member_user = null;
                                        var store_path = path.join(os.homedir(), '.hfc-key-store');
                                        console.log(' Store path:' + store_path);
                                        var tx_id = null;
                                        // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
                                        Fabric_Client.newDefaultKeyValueStore({
                                            path: store_path
                                        }).then((state_store) => {
                                            // assign the store to the fabric client
                                            fabric_client.setStateStore(state_store);
                                            var crypto_suite = Fabric_Client.newCryptoSuite();
                                            // use the same location for the state store (where the users' certificate are kept)
                                            // and the crypto store (where the users' keys are kept)
                                            var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
                                            crypto_suite.setCryptoKeyStore(crypto_store);
                                            fabric_client.setCryptoSuite(crypto_suite);
                                            // get the enrolled user from persistence, this user will sign all requests
                                            return fabric_client.getUserContext('saji', true);
                                        }).then((user_from_store) => {
                                            if (user_from_store && user_from_store.isEnrolled()) {
                                                console.log('Successfully loaded saji from persistence');
                                                member_user = user_from_store;
                                            } else {
                                                throw new Error('Failed to get saji.... run registerUser.js');
                                            }
                                            // get a transaction id object based on the current user assigned to fabric client
                                            tx_id = fabric_client.newTransactionID();
                                            console.log("Assigning transaction_id: ", tx_id._transaction_id);
                                            var claim_id = userEmail + vehicleNo
                                            // must send the proposal to endorsing peers
                                            var request = {
                                                //targets: let default to the peer assigned to the client
                                                chaincodeId: 'register',
                                                fcn: 'claimDetails',
                                                args: [vehicleNo, userEmail, claim_initiated, Incident_Date, claim_reason, claim_reason_image, claim_status,vehicleData[0], providerName, claim_id,reason_for_rejection],
                                                chainId: 'mychannel',
                                                txId: tx_id
                                            };


                                            // send the transaction proposal to the peers
                                            return channel.sendTransactionProposal(request);
                                        }).then((results) => {
                                            var proposalResponses = results[0];
                                            var proposal = results[1];
                                            let isProposalGood = false;
                                            if (proposalResponses && proposalResponses[0].response &&
                                                proposalResponses[0].response.status === 200) {
                                                isProposalGood = true;
                                                console.log('Transaction proposal was good');
                                            } else {
                                                console.error('Transaction proposal was bad');
                                            }
                                            if (isProposalGood) {
                                                console.log(util.format(
                                                    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                                                    proposalResponses[0].response.status, proposalResponses[0].response.message));

                                                // build up the request for the orderer to have the transaction committed
                                                var request = {
                                                    proposalResponses: proposalResponses,
                                                    proposal: proposal
                                                };

                                                // set the transaction listener and set a timeout of 30 sec
                                                // if the transaction did not get committed within the timeout period,
                                                // report a TIMEOUT status
                                                var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
                                                var promises = [];

                                                var sendPromise = channel.sendTransaction(request);
                                                promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

                                                // get an eventhub once the fabric client has a user assigned. The user
                                                // is required bacause the event registration must be signed
                                                let event_hub = channel.newChannelEventHub(peer);

                                                // using resolve the promise so that result status may be processed
                                                // under the then clause rather than having the catch clause process
                                                // the status
                                                let txPromise = new Promise((resolve, reject) => {
                                                    let handle = setTimeout(() => {
                                                        event_hub.unregisterTxEvent(transaction_id_string);
                                                        event_hub.disconnect();
                                                        resolve({ event_status: 'TIMEOUT' }); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
                                                    }, 3000);
                                                    event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                                                        // this is the callback for transaction event status
                                                        // first some clean up of event listener
                                                        clearTimeout(handle);

                                                        // now let the application know what happened
                                                        var return_status = { event_status: code, tx_id: transaction_id_string };
                                                        if (code !== 'VALID') {
                                                            console.error('The transaction was invalid, code = ' + code);
                                                            resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                                                        } else {
                                                            console.log('The transaction has been committed on peer ' + event_hub.getPeerAddr());
                                                            resolve(return_status);
                                                        }
                                                    }, (err) => {
                                                        //this is the callback if something goes wrong with the event registration or processing
                                                        reject(new Error('There was a problem with the eventhub ::' + err));
                                                    },
                                                        { disconnect: true } //disconnect when complete
                                                    );
                                                    event_hub.connect();

                                                });
                                                promises.push(txPromise);

                                                return Promise.all(promises);
                                            } else {
                                                console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
                                                throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
                                            }
                                        }).then((results) => {
                                            console.log('Send transaction promise and event listener promise have completed');
                                            // check the results in the order the promises were added to the promise all list
                                            if (results && results[0] && results[0].status === 'SUCCESS') {
                                                console.log('Successfully sent transaction to the orderer.');
                                            } else {
                                                console.error('Failed to order the transaction. Error code: ' + results[0].status);
                                            }

                                            if (results && results[1] && results[1].event_status === 'VALID') {
                                                console.log('Successfully committed the change to the ledger by the peer');
                                            } else {
                                                console.log('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
                                                res.status(500).json({ message: 'Transaction failed to be committed to the ledger due to ::' + results[1].event_status })
                                            }
                                        }).catch((err) => {
                                            console.error('Failed to invoke successfully :: ' + err);
                                            res.status(400).json({ err: 'Failed to invoke successfully :: ' + err });
                                        });
                                        // :::::::::::::::::: Blockchain part ends here ::::::::::::::::::::::::::::::::::

                                        res.status(200).json({
                                            message: 'Claiming request is sent'
                                        });
                                    })
                                    .catch(error => {
                                        console.log('error saving data:', error);
                                        res.status(200).json({
                                            vehicleData: data,
                                            message: 'entry not added to the table'
                                        });
                                    });

                                } else {
                                console.log('Invalid Date')
                            res.status(401).json({
                                status:401,
                                message: 'Invalid Date'
                            });
                            }
                        }
                    
                            
                    
                        } else {
                            if(purchased_date < Date.parse(Incident_Date) && Date.parse(Incident_Date) <= Date.now()){
                            claimInsuranceObj
                                .save()
                                .then(() => {

                                    var user_profiles = vehicleData[0].insurance_plans;

                                    // :::::::::::::::::: Blockchain part starts here:::::::::::::::::::::::::::::::::

                                    var fabric_client = new Fabric_Client();
                                    // setup the fabric network
                                    var channel = fabric_client.newChannel('mychannel');
                                    var peer = fabric_client.newPeer('grpc://localhost:7051');
                                    channel.addPeer(peer);
                                    var order = fabric_client.newOrderer('grpc://localhost:7050')
                                    channel.addOrderer(order);
                                    //
                                    var member_user = null;
                                    var store_path = path.join(os.homedir(), '.hfc-key-store');
                                    console.log(' Store path:' + store_path);
                                    var tx_id = null;
                                    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
                                    Fabric_Client.newDefaultKeyValueStore({
                                        path: store_path
                                    }).then((state_store) => {
                                        // assign the store to the fabric client
                                        fabric_client.setStateStore(state_store);
                                        var crypto_suite = Fabric_Client.newCryptoSuite();
                                        // use the same location for the state store (where the users' certificate are kept)
                                        // and the crypto store (where the users' keys are kept)
                                        var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
                                        crypto_suite.setCryptoKeyStore(crypto_store);
                                        fabric_client.setCryptoSuite(crypto_suite);
                                        // get the enrolled user from persistence, this user will sign all requests
                                        return fabric_client.getUserContext('saji', true);
                                    }).then((user_from_store) => {
                                        if (user_from_store && user_from_store.isEnrolled()) {
                                            console.log('Successfully loaded saji from persistence');
                                            member_user = user_from_store;
                                        } else {
                                            throw new Error('Failed to get saji.... run registerUser.js');
                                        }
                                        // get a transaction id object based on the current user assigned to fabric client
                                        tx_id = fabric_client.newTransactionID();
                                        console.log("Assigning transaction_id: ", tx_id._transaction_id);
                                        var claim_id = userEmail + vehicleNo
                                        // must send the proposal to endorsing peers
                                        var request = {
                                            //targets: let default to the peer assigned to the client
                                            chaincodeId: 'register',
                                            fcn: 'claimDetails',
                                            args: [vehicleNo, userEmail, claim_initiated, Incident_Date, claim_reason, claim_reason_image, claim_status, user_profiles.toString(), providerName, claim_id],
                                            chainId: 'mychannel',
                                            txId: tx_id
                                        };


                                        // send the transaction proposal to the peers
                                        return channel.sendTransactionProposal(request);
                                    }).then((results) => {
                                        var proposalResponses = results[0];
                                        var proposal = results[1];
                                        let isProposalGood = false;
                                        if (proposalResponses && proposalResponses[0].response &&
                                            proposalResponses[0].response.status === 200) {
                                            isProposalGood = true;
                                            console.log('Transaction proposal was good');
                                        } else {
                                            console.error('Transaction proposal was bad');
                                        }
                                        if (isProposalGood) {
                                            console.log(util.format(
                                                'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                                                proposalResponses[0].response.status, proposalResponses[0].response.message));

                                            // build up the request for the orderer to have the transaction committed
                                            var request = {
                                                proposalResponses: proposalResponses,
                                                proposal: proposal
                                            };

                                            // set the transaction listener and set a timeout of 30 sec
                                            // if the transaction did not get committed within the timeout period,
                                            // report a TIMEOUT status
                                            var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
                                            var promises = [];

                                            var sendPromise = channel.sendTransaction(request);
                                            promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

                                            // get an eventhub once the fabric client has a user assigned. The user
                                            // is required bacause the event registration must be signed
                                            let event_hub = channel.newChannelEventHub(peer);

                                            // using resolve the promise so that result status may be processed
                                            // under the then clause rather than having the catch clause process
                                            // the status
                                            let txPromise = new Promise((resolve, reject) => {
                                                let handle = setTimeout(() => {
                                                    event_hub.unregisterTxEvent(transaction_id_string);
                                                    event_hub.disconnect();
                                                    resolve({ event_status: 'TIMEOUT' }); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
                                                }, 3000);
                                                event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                                                    // this is the callback for transaction event status
                                                    // first some clean up of event listener
                                                    clearTimeout(handle);

                                                    // now let the application know what happened
                                                    var return_status = { event_status: code, tx_id: transaction_id_string };
                                                    if (code !== 'VALID') {
                                                        console.error('The transaction was invalid, code = ' + code);
                                                        resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                                                    } else {
                                                        console.log('The transaction has been committed on peer ' + event_hub.getPeerAddr());
                                                        resolve(return_status);
                                                    }
                                                }, (err) => {
                                                    //this is the callback if something goes wrong with the event registration or processing
                                                    reject(new Error('There was a problem with the eventhub ::' + err));
                                                },
                                                    { disconnect: true } //disconnect when complete
                                                );
                                                event_hub.connect();

                                            });
                                            promises.push(txPromise);

                                            return Promise.all(promises);
                                        } else {
                                            console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
                                            throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
                                        }
                                    }).then((results) => {
                                        console.log('Send transaction promise and event listener promise have completed');
                                        // check the results in the order the promises were added to the promise all list
                                        if (results && results[0] && results[0].status === 'SUCCESS') {
                                            console.log('Successfully sent transaction to the orderer.');
                                        } else {
                                            console.error('Failed to order the transaction. Error code: ' + results[0].status);
                                        }

                                        if (results && results[1] && results[1].event_status === 'VALID') {
                                            console.log('Successfully committed the change to the ledger by the peer');
                                        } else {
                                            console.log('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
                                            res.status(500).json({ message: 'Transaction failed to be committed to the ledger due to ::' + results[1].event_status })
                                        }
                                    }).catch((err) => {
                                        console.error('Failed to invoke successfully :: ' + err);
                                        res.status(400).json({ err: 'Failed to invoke successfully :: ' + err });
                                    });
                                    // :::::::::::::::::: Blockchain part ends here ::::::::::::::::::::::::::::::::::

                                    res.status(200).json({
                                        message: 'Claiming request is sent'
                                    });
                                })
                                .catch(error => {
                                    console.log('error saving data:', error);
                                    res.status(200).json({
                                        vehicleData: vehicleData,
                                        message: 'entry not added to the table'
                                    });
                              });
                            }
                            else{
                                res.status(401).json({
                                    status:401,
                                    message: 'Invalid Date'
                                });
                            }
                        }
                    })
                    .catch(error => {
                        res.status(400).json({
                            message: 'Vehicle Number Not Found!',
                            error: error
                        });
                    });
            } else {
                console.log('No vehicle found for the given number');
            }
        })
        .catch(error => {
            res.status(400).json({
                message: 'Vehicle Number Not Found!',
                error: error
            });
        });
};

exports.getClaimStatus = (req, res, next) => {
    ClaimSchema
        .find({ email: req.query.email })
        .populate({ path: 'user_profile' })
        .exec()
        .then(claimStatusData => {

            res.status(200).json({
                message: 'Claim Status !',
                vehicle_data: claimStatusData
            });
        })
        .catch(error => {
            res.status(400).json({
                message: 'Vehicle Number Not Found!',
                error: error
            });
        });
}
exports.getClaimStatusBC = (req, res, next) => {
    // :::::::::::::::::: Blockchain part starts here:::::::::::::::::::::::::::::::::
    var fabric_client = new Fabric_Client();

    // setup the fabric network
    var channel = fabric_client.newChannel('mychannel');
    var peer = fabric_client.newPeer('grpc://localhost:7051');
    channel.addPeer(peer);
    //
    var member_user = null;
    var store_path = path.join(os.homedir(), '.hfc-key-store');
    console.log(' Store path:' + store_path);
    var tx_id = null;

    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({
        path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext('saji', true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded saji from persistence', user_from_store);
            member_user = user_from_store;
        } else {
            throw new Error('Failed to get saji.... run registerUser.js');
        }

        const request = {
            //targets : --- letting this default to the peers assigned to the channel
            chaincodeId: 'register',
            fcn: 'getuserClaims',
            args: [req.query.claim_id]
        };

        // send the query proposal to the peer
        return channel.queryByChaincode(request);
    }).then((query_responses) => {
        console.log("Query has completed, checking results");
        // query_responses could have more than one  results if there multiple peers were used as targets
        if (query_responses && query_responses.length == 1) {
            if (query_responses[0] instanceof Error) {
                console.error("error from query = ", query_responses[0]);
                res.status(400).json({ message: "error from query = " + query_responses[0] })
            } else {
                console.log("Response is ", query_responses[0].toString());
                var blockdata = JSON.parse(query_responses[0].toString())
                res.status(200).json({
                    message: 'data in blockchain',
                    payload: blockdata
                })
            }
        } else {
            console.log("No payloads were returned from query");
            res.status(400).json({ message: "No payloads were returned from query" })
        }
    }).catch((err) => {
        console.error('Failed to query successfully :: ' + err);
        res.status(400).json({ message: 'Failed to query successfully :: ' + err })
    });
    // :::::::::::::::::: Blockchain part ends here ::::::::::::::::::::::::::::::::::

}
