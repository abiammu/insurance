const mongoose = require('mongoose');
const ClaimModel = require('../../models/user-claim-insurance-status');

exports.getClaimInsuranceVehicleData = (req, res, next) => {

    ClaimModel
        .find({ claim_initiated: 'yes', claim_status: 'pending' })
        .exec()
        .then(claimData => {
            console.log(claimData);
            res.status(200).json({
                message: 'Claim Request Data',
                claimData: claimData
            
            });
            
        })
        .catch(error => {
            res.status(400).json({
                message: 'No Claim Request',
                error: error
            });
        });
};
exports.getAllApprovedClaims = (req, res, next) => {
    ClaimModel
        .find({claim_status:'approved'})
        .exec()
        .then(claimData => {
            res.status(200).json({
                message: 'Claim Data',
                claimData: claimData
            });
        
        })
        .catch(error => {
            res.status(400).json({
                message: 'No Claim Request',
                error: error
            });
        });
};
exports.getAllRejectedClaims = (req, res, next) => {
    ClaimModel
        .find({claim_status:'rejected'})
        .exec()
        .then(claimData => {
            console.log(claimData)
            res.status(200).json({
                message: 'Claim Data',
                claimData: claimData
            });
        
        })
        .catch(error => {
            res.status(400).json({
                message: 'No Claim Request',
                error: error
            });
        });
};