const mongoose = require('mongoose');
const insuredItem = mongoose.model('UserInsure');
const ClaimSchema = require('../../models/user-claim-insurance-status');

var userInsureSchema = require('../../models/user-insure');

var InsurerAddCategorySchema = require('../../models/insurer-add-category');

var InsurerSchema = require('../../models/insurer-signup');

const plansSchema = mongoose.model('InsurerAddPlans');
var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var os = require('os');

exports.getClaims = (req, res, next) => {
    console.log(req.query.email)
    InsurerSchema.find({ email: req.query.email})
    .exec()
    .then(findUser => {
        console.log(findUser);
       var provider=findUser[0].orgName;
    ClaimSchema
    .find({provider_name:provider})
        .populate({path: 'user_profile'})
        .exec()
        .then(claimStatusData => {

            console.log(claimStatusData);
            res.status(200).json({
                message: 'Claim Status !',
                vehicle_data: claimStatusData
            });
        })
        .catch(error => {
            res.status(400).json({
                message: 'Vehicle Number Not Found!',
                error: error
            });
        });
    })    
}


exports.getClaimsBC = (req, res, next) => {
    var email = req.query.email;
    InsurerSchema.find({ email: req.query.email})
    .exec()
    .then(findUser => {
        console.log(findUser);
       var provider=findUser[0].orgName;
       console.log(provider);
       var fabric_client = new Fabric_Client();
      
       // setup the fabric network
       var channel = fabric_client.newChannel('mychannel');
       var peer = fabric_client.newPeer('grpc://localhost:7051');
       channel.addPeer(peer);
       console.log('hello')
       var member_user = null;
       var store_path = path.join(os.homedir(), '.hfc-key-store');
       console.log(' Store path:' + store_path);
       var tx_id = null;
       console.log('hello2')
       // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
       Fabric_Client.newDefaultKeyValueStore({
           path: store_path
       }).then((state_store) => {
           // assign the store to the fabric client
           fabric_client.setStateStore(state_store);
           var crypto_suite = Fabric_Client.newCryptoSuite();
           // use the same location for the state store (where the users' certificate are kept)
           // and the crypto store (where the users' keys are kept)
           var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
           crypto_suite.setCryptoKeyStore(crypto_store);
           fabric_client.setCryptoSuite(crypto_suite);
   
           // get the enrolled user from persistence, this user will sign all requests
           return fabric_client.getUserContext(email, true);
       }).then((user_from_store) => {
           if (user_from_store && user_from_store.isEnrolled()) {
               console.log('Successfully loaded '+email+' from persistence',user_from_store);
               member_user = user_from_store;
           } else {
               throw new Error('Failed to get '+email+'.... run registerUser.js');
           }
   
           const request = {
               //targets : --- letting this default to the peers assigned to the channel
               chaincodeId: 'register',
               fcn: 'queryAllemail',
               args: [provider]
           };
           console.log(request)
           // send the query proposal to the peer
           return channel.queryByChaincode(request);
       }).then((query_responses) => {`1`
           console.log("Query has completed, checking results");
           // query_responses could have more than one  results if there multiple peers were used as targets
           if (query_responses && query_responses.length == 1) {
               if (query_responses[0] instanceof Error) {
                   console.error("error from query = ", query_responses[0]);
                   res.status(400).json({message:"error from query = "+query_responses[0]})
               } else {
                   console.log("Response is ", query_responses[0].toString());
                   var blockdata=JSON.parse(query_responses[0].toString())
               res.status(200).json({
                   message:'data in blockchain',
                   payload:blockdata
               })
               }
           } else {
               console.log("No payloads were returned from query");
               res.status(400).json({message:"No payloads were returned from query"})
           }
       }).catch((err) => {
           console.error('Failed to query successfully :: ' + err);
           res.status(400).json({message:'Failed to query successfully :: ' + err})
       });
   
    }) .catch(error => {
        res.status(400).json({
            message: 'Vehicle Number Not Found!',
            error: error
        });
    });   
}






// console.log(req.query.email)
// InsurerSchema.findOne({ email: req.query.email }).exec().then((insurerdata) => {
//     var provider_name = insurerdata.orgName;
//     console.log(provider_name)
//     InsurerAddCategorySchema.find({ insurance_provider_name: provider_name }).exec().then((categorydata) => {
//         var id = categorydata[0]._id;
//         plansSchema.find({ insurance_category: id }).populate({ path: 'insurance_category' })
//             .exec()
//             .then(plandata => {
//                 //console.log(plandata)
//                 var planid = [];
//                 for (var i = 0; i < plandata.length; i++) {
//                     //console.log(plandata[i]._id)
//                     planid.push(plandata[i]._id)
//                 }
//                 console.log(planid.length, planid)
                
//                 for (var j = 0; j < planid.length; j++) {
//                     userInsureSchema.find({ insurance_plans: planid[j] })
//                         .exec()
//                         .then(insured => {
//                             console.log(insured.length,insured)
//                             for (var k = 0; k < insured.length; k++) {
                                

//                                 ClaimSchema.find({ user_profile: insured[k]._id })
//                                     .exec()
//                                     .then(claimStatusData => {
//                                         console.log(claimStatusData);
//                                     })
//                             }


//                         })
//                 }
//             })
//     })
//         .catch(error => {
//             res.status(400).json({
//                 message: 'Vehicle Number Not Found!',
//                 error: error
//             });
//         });
// })

