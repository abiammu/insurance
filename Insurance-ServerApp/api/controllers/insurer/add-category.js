var express = require('express');
var app = express();
var mongoose = require('mongoose');
var InsurerAddCategorySchema = require('../../models/insurer-add-category');

// make first letter capital 
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
exports.insurerAddCategory = function(req, res, next) {
    var insurance_provider_name=req.body.insurance_provider_name.toUpperCase();
    var category=capitalizeFirstLetter(req.body.category.toLowerCase());
    var email = req.query.email;
    const insurerAddCategory = new InsurerAddCategorySchema({
        _id: new mongoose.Types.ObjectId(),
        insurance_provider_name:insurance_provider_name,
        category: category, 
        email:email,       
        createdAt: Date.now()
    });
console.log(category)
    InsurerAddCategorySchema.find({ insurance_provider_name:insurance_provider_name, category: category,email:email})
        .exec()
        .then(addCategory => {
            console.log('add', addCategory);
            if (addCategory.length >= 1) {
                return res.status(409).json({
                    message: 'category Already Added !',
                    status:409
                });
            } else {
                insurerAddCategory
                    .save()
                    .then(result => {
                        console.log('Category Added Successful !');
                        res.status(200).json({
                            message: 'Category Added Successful !',
                            payload:result
                        });
                    })
                    .catch(error => {
                        res.status(500).json({
                            message: 'Adding Category Failed !',
                            error: error
                        });
                    });
            }
        })
        .catch(error => {
            res.status(500).json({
                message: 'failed to get Insurance Category',
                error: error,
                status:500
            });
        });
};
exports.getCategory = function(req,res,next){
    var email=req.query.email;
    InsurerAddCategorySchema.find({email:email},'category').exec().then(category=>{
        res.status(200).json({
            message: 'Category !',
            payload : category
        });

    }).catch(error=>{
        res.status(500).json({
            message: 'failed to get Insurance Category',
            error: error,
            status:500
        });
    })
}