var express = require('express');
var app = express();
var mongoose = require('mongoose');
var InsurerAddPlansSchema = require('../../models/insurer-add-plans');
var InsurerAddCategorySchema = require('../../models/insurer-add-category');

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
exports.insurerAddplans = function(req, res, next) {
    var insCategory =capitalizeFirstLetter(req.body.category.toLowerCase());
    var insuranceProvider = req.body.insurance_provider_name.toUpperCase();
    console.log(insCategory, insuranceProvider);

    InsurerAddCategorySchema.find({
        category: insCategory,
        insurance_provider_name: insuranceProvider
    })
        .exec()
        .then(category => {
            console.log(category);
            if (category.length >= 1) {
                const insurerAddPlans = new InsurerAddPlansSchema({
                    _id: new mongoose.Types.ObjectId(),
                    insurance_for: req.body.insurance_for.toLowerCase(),
                    insurance_provider_name:insuranceProvider,
                    insurance_plan_year: req.body.insurance_plan_year,
                    yearly_premium: req.body.yearly_premium,
                    about_insurance_plan: req.body.about_insurance_plan,
                    insurance_category: category[0]._id,
                    createdAt: Date.now()
                });

                // Finding if Plans already exists
                InsurerAddPlansSchema.find({
                    insurance_for: req.body.insurance_for.toLowerCase(),
                    insurance_plan_year: req.body.insurance_plan_year,
                    insurance_provider_name:insuranceProvider
                })
                    .exec()
                    .then(plans => {
                        console.log('Plans ', plans);
                        if (plans.length >= 1) {
                            return res.status(409).json({
                                message: 'Plans Already Added !',
                                status: 409
                            });
                        } else {
                            insurerAddPlans
                                .save()
                                .then(result => {
                                    console.log('Plans Added Successful !');
                                    res.status(200).json({
                                        message: 'Plans Added Successful !'
                                    });
                                })
                                .catch(error => {
                                    res.status(500).json({
                                        message: 'Adding Plans Failed !',
                                        error: error
                                    });
                                });
                        }
                    })
                    .catch(error => {
                        res.status(500).json({
                            message: 'failed to get Insurance Plans',
                            error: error
                        });
                    });
            } else {
                res.status(500).json({
                    message: 'failed to get Insurance Plans',
                    error: error
                });
            }
        })
        .catch(error => {
            res.status(500).json({
                message: 'Category not found to map in Plans',
                error: error,
                status: 500
            });
        });
};
