/* 'use strict';

var mongoose = require('mongoose');
var insurancePlans = mongoose.model('insurancePlans');

// adding category to database

exports.AddCategory = function (req, res) {

  var insuranceType = req.body.insuranceType;

  var new_insurance = new insurancePlans({
    insuranceType: insuranceType
  });

  new_insurance.save({}, function (err, data) {
    if (err) {
      res.status(404).json({
        message: err.message,
      })
    }
    else {
      res.status(200).json({ message: data })
    }

  })
}

// getting all categories from database

exports.getcategory = function (req, res) {

  //select ('-_id')=>removes id column when displaying

  insurancePlans.find({}).select('-_id')

    .then((data) => {
      res.status(200).json({
        message: data,
      })
    })
    .catch((err) => {
      res.status(404).json({
        message: "cannot get data",
        err: err.message
      })
    })
}

// (only works after running category api) adding plans to category

exports.addPlans = function (req, res) {

  var insuranceType = req.body.insuranceType,
    typeOfVehicle = req.body.typeOfVehicle,
    provider_company = req.body.provider,

    //adding data to existing category

    update = { $set: { typeOfVehicle: typeOfVehicle, provider_company: provider_company } };

  insurancePlans.updateOne({ insuranceType: insuranceType, typeOfVehicle: null }, update).then((data) => {
    if (data.n == "0") {
      res.status(404).json({
        message: "sorry category not found"
      })

    } else {
      res.status(200).json({
        message: "added plans sucessfully to the category",
      })
    }
  })
    .catch((err) => {
      res.status(404).json({
        message: "cannot get data",
        err: err.message
      })
    })
}
//(only works after adding plans) adding schemes into the plans

exports.addschemes = function (req, res) {
  var insuranceType = req.body.insuranceType,
    typeOfVehicle = req.body.typeOfVehicle,
    provider_company = req.body.provider,
    claim = req.body.claim,
    payable = req.body.payable,
    premium = req.body.premium,

    //adding scheme data to schemes column

    update = { $set: { schemes: { claim: claim, payable: payable, premium: premium } } };


  insurancePlans.updateOne({
    insuranceType: insuranceType, typeOfVehicle: typeOfVehicle,
    provider_company: provider_company
     }, update).then((data) => {
      if(data.n=="0"){
        res.status(404).json({
          message:'plans not found to add schemes',
        })
      }else if(data.nModified=="1"&& data.n=="1"){

        res.status(200).json({
          message:"schemes modified and added to plans sucessfully"
        
        })

      }else if(data.n=="1"){

        res.status(200).json({
          message:"schemes added to plans sucessfully",
          
        
        })
      }
    
  }).catch((err) => {
    res.status(404).json({
      message: "cannot get data",
      err: err.message
    })
  })

}

// }}else if(operation==='delete'){
//   var typeOfInsurance = req.body.typeOfInsurance;
//   mongoose.connection.db.dropCollection(typeOfInsurance).then((data) => {
//     res.status(200).json({
//       message: 'deleted  collection sucessfully',
//       payload:typeOfInsurance
//     });
//   }).catch((err) => {
//     res.json({
//       message: 'no data found for the collection name',

//     });
//   })
// }}; */