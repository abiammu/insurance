'use strict';

//importing mongoose 
var mongoose = require('mongoose');
var LocalStorage = require('node-localstorage').LocalStorage;

//initiating TABLE to Insurence
var signup = require('../../models/signup');
var verifyemailId = require('../../models/verification');
const bcrypt = require('bcryptjs');

exports.userSignin = function (req, res) {
  var email = req.body.email.toLowerCase();
  var password = req.body.password;

  //Getting parameters from the user
  //using parameters getting the userDetails from dataBase

  signup.findOne({ email: email })
    .then((data) => {

      bcrypt.compare(
        password, data.password,
        (err, bcryptRes) => {
          //if any error in bcryption 
          if (err) {
            return res.status(401).json({
              message: 'password doesn\'t match',
              error: err.message,
              status: 401
            });
          }
          // if password matches with the user Input 
          if (bcryptRes == true) {

            verifyemailId.findOne({ emailId: email }).then(verify => {
              console.log(verify);
              if (verify.isVerified == true) {

                if (typeof localStorage === "undefined" || localStorage === null) {
                 var localStorage = new LocalStorage('./local');
                }
                email.replace("%40", "@");
                localStorage.removeItem(email);

                //if user insurence page
                if (data.role == 'insurance_holder') {
                  console.log(data.role);

                  res.status(200).json({
                    userData: data,
                    //email: data.email,
                    status: 201
                  });

                  //if Admin Admin page
                }
                // if Admin Admin page
                else if (data.role == 'insurance_provider') {
                  console.log(data.role);
                  res.status(200).json({
                    userData: data,
                    status: 202
                  });
                } else if (data.role == 'inspector') {
                  console.log(data.role);
                  res.status(200).json({
                    userData: data,
                  });
                }

              }
              else {
                res.status(402).json({
                  message: 'EmailID is not verified yet',
                  status: 402
                });
              }
            })


          }
          //handleing password not match error
          else if (bcryptRes == false) {
            return res.status(401).json({
              message: 'Wrong password please try again',
              status: 401
            });
          }
        })

      // Handeling user not found error
    })
    .catch((error) => {
      res.status(404).json({
        message: 'user not found',
        error: error.message
      });
    })
};
