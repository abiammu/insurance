var verifyOtp = require('../../models/verification');
var LocalStorage = require('node-localstorage').LocalStorage;

exports.verifyotp = function(req,res){
    var email = req.query.email.toLowerCase(),
             otp = req.body.otp;
             if (typeof localStorage === "undefined" || localStorage === null) {
               
                localStorage = new LocalStorage('./local');
            }
            email.replace("%40", "@");
            var otpvalue = localStorage.getItem(email);

             verifyOtp.findOne({emailId:email}).then(data=>{
                console.log("update successfully");
                 if(otpvalue == otp){
                    var update ={ $set: { isVerified : true}}
                     verifyOtp.updateOne({emailId:email},update).then(result=>{
                         console.log("update successfully");
                       return res.status(202).json({
                            message: 'verified successfully',
                            status: 202
                        });
                     }).catch(err=>{
                        res.status(401).json({
                            message: err.message,
                            status: 401
                     });
                 });
                }
                else{
                    res.status(409).json({
                        message: 'Incorrect OTP',
                        status: 409
                });
            }
             }).catch(err=>{
                res.status(401).json({
                    message: err.message,
                    status: 401
             });
             })
}