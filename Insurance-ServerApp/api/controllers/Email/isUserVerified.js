var verifyOtp = require('../../models/verification');

exports.isUserVerified = function (req, res) {
    var email = req.query.email.toLowerCase();
    console.log('emailid::',email)
    verifyOtp.findOne({ emailId: email },'isVerified').then(data => {
        console.log("emailid present", data);

        res.status(200).json({
            data: data
        });
        
    }).catch(err => {
        res.status(401).json({
            message: 'email does not exist',
            error: err
        });
    })
}