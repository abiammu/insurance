var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var Schema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password:  { type: String, required: true },
    role: { type: String, required: true },
    image: {
        type: String,
        required: true
    },
    orgName: {type: String},
    created_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Signup', Schema,'Signup');
