var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Schema = new Schema({
  username:{type:String,required:true,match:/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/},
  model: Number,
    brand: Number,
    vehicleNumber:{type: String, required: true},
  premium: String,
  insuranceAmount: Number,
  sumAmount:Number,
  created_at: { type: Date, default: Date.now }   
  });

module.exports = mongoose.model('InsurancePremium', Schema,'InsurancePremium');