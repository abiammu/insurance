var mongoose = require('mongoose');
var schema = mongoose.Schema;

var insurerAddPlansSchema = new schema({
    _id: mongoose.Schema.Types.ObjectId,
    insurance_for: {
        type: String,
        required: true
    },insurance_provider_name:{
        type: String,
        required: true
    },
    insurance_plan_year: {
        type: String,
        required: true
    },
    
    yearly_premium:{
        type: String,
        required: true
    },
    about_insurance_plan:{
        type: String,
        required: true
    },
    insurance_category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'InsurerAddCategory'
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('InsurerAddPlans', insurerAddPlansSchema);