var mongoose = require('mongoose');
var schema = mongoose.Schema;

var insurerSignUpSchema = new schema({
    _id: mongoose.Schema.Types.ObjectId,
    orgName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    contact: {
        type: String
    },
    role: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('InsuranceProviderSignup', insurerSignUpSchema,'InsuranceProviderSignup');