var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var http = require('http');
var fs = require('fs');
var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var os = require('os');
var blockLength = '';
var DEBUG = 1;

// Console.log function


module.exports = (function () {

   return {
      getUser: function (req, res) {
         var email = req.query.email;
         console.log(email)
         var fabric_client = new Fabric_Client();

         // setup the fabric network
         var channel = fabric_client.newChannel('mychannel');
         var peer = fabric_client.newPeer('grpc://localhost:7051');
         channel.addPeer(peer);

         //
         var member_user = null;
         var store_path = path.join(os.homedir(), '.hfc-key-store');
         console.log('Store path:' + store_path);
         var tx_id = null;

         // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
         Fabric_Client.newDefaultKeyValueStore({
            path: store_path
         })
            .then(state_store => {
               // assign the store to the fabric client
               fabric_client.setStateStore(state_store);
               var crypto_suite = Fabric_Client.newCryptoSuite();
               // use the same location for the state store (where the users' certificate are kept)
               // and the crypto store (where the users' keys are kept)
               var crypto_store = Fabric_Client.newCryptoKeyStore({
                  path: store_path
               });
               crypto_suite.setCryptoKeyStore(crypto_store);
               fabric_client.setCryptoSuite(crypto_suite);

               // get the enrolled user from persistence, this user will sign all requests
               return fabric_client.getUserContext(email, true);
            })
            .then(user_from_store => {
               if (user_from_store && user_from_store.isEnrolled()) {
                  member_user = JSON.parse(user_from_store);

                  certificate = member_user.enrollment.identity.certificate

                  res.status(200).json({
                     user: member_user.name,
                     certificate: member_user.enrollment.identity.certificate,
                     signin_certificate: member_user.enrollment.signingIdentity,
                  })
               } else {
                  throw new Error(
                     'Failed to get' + email + '.... run registerUser.js'
                  );
               }
            })
            .catch(err => {
               console.error('Failed to find block details :: ' + err);
               res.status(200).json({
                  err: err.message
               })
            });
      }
   };
})();
